package com.jaka.framwork.core.dingding.notice;

import com.jaka.framework.core.dingding.DingTalkAPIClientBuilder;
import com.jaka.framework.core.dingding.api.notice.input.TopApiMessageCorpConverSationAsyncSendV2Input;
import com.jaka.framework.core.dingding.api.notice.input.message.*;
import com.jaka.framework.core.dingding.api.notice.result.TopApiMessageCorpConverSationAsyncSendV2Result;
import com.jaka.framework.core.dingding.base.en.RequestMappering;
import org.junit.Test;

/**
 * @author ：james.liu
 * @date ：Created in 2021/12/29 9:47
 * @description：
 * @version: 1.0
 */
public class TestNotice {


    @Test
    public void topapi_message_corpconversation_asyncsend_v2_text() throws Exception {
        TopApiMessageCorpConverSationAsyncSendV2Input input = new TopApiMessageCorpConverSationAsyncSendV2Input();
        input.setCmdId(RequestMappering.top_api_message_corp_conver_sation_async_send_v2.getMapperName());
        input.setAgentId(1338259958L);
        input.setUseridList("16393588697541721");
        NoticeText noticeText = new NoticeText();
        noticeText.setMsgtype(MsgtypeEnums.text);
        NoticeText.Text text = new NoticeText.Text();
        text.setContent("今天是周三，给你一个幸福的通知");
        noticeText.setText(text);
        input.setMsg(noticeText);
        TopApiMessageCorpConverSationAsyncSendV2Result result = new DingTalkAPIClientBuilder()
                .setAccessToken("442981d404033db781b79f5abf7a165e")
                .build()
                .getNoticeClient()
                .topApiMessageCorpConverSationAsyncSendV2(input);
        System.out.println(result.toJsonString());
    }

    @Test
    public void topapi_message_corpconversation_asyncsend_v2_image() throws Exception {
        TopApiMessageCorpConverSationAsyncSendV2Input input = new TopApiMessageCorpConverSationAsyncSendV2Input();
        input.setCmdId(RequestMappering.top_api_message_corp_conver_sation_async_send_v2.getMapperName());
        input.setAgentId(1338259958L);
        input.setUseridList("16393588697541721");
        NoticeImage noticeImage = new NoticeImage();
        noticeImage.setMsgtype(MsgtypeEnums.image);
        NoticeImage.Image image = new NoticeImage.Image();
        image.setMedia_id("");
        input.setMsg(noticeImage);
        TopApiMessageCorpConverSationAsyncSendV2Result result = new DingTalkAPIClientBuilder()
                .setAccessToken("1bdb11757f9b3627812e948af15f36d1")
                .build()
                .getNoticeClient()
                .topApiMessageCorpConverSationAsyncSendV2(input);
        System.out.println(result.toJsonString());
    }


    @Test
    public void topapi_message_corpconversation_asyncsend_v2_oa() throws Exception {
        TopApiMessageCorpConverSationAsyncSendV2Input input = new TopApiMessageCorpConverSationAsyncSendV2Input();
        input.setCmdId(RequestMappering.top_api_message_corp_conver_sation_async_send_v2.getMapperName());
        input.setAgentId(1338259958L);
        input.setUseridList("16393588697541721");
        NoticeOA noticeOA = new NoticeOA();
        noticeOA.setMsgtype(MsgtypeEnums.oa);
        NoticeOA.OA oa = new NoticeOA.OA();
        NoticeOA.Head head = new NoticeOA.Head();
        head.setBgcolor("FFBBBBBB");
        head.setText("头部标题");
        oa.setHead(head);
        NoticeOA.Body body = new NoticeOA.Body();
        body.setTitle("xxxx");
        body.setContent("xxxxxxxxxxxxx");
        body.setAuthor("james");
        oa.setBody(body);
        oa.setMessage_url("http://www.baidu.com");
        noticeOA.setOa(oa);
        input.setMsg(noticeOA);
        TopApiMessageCorpConverSationAsyncSendV2Result result = new DingTalkAPIClientBuilder()
                .setAccessToken("442981d404033db781b79f5abf7a165e")
                .build()
                .getNoticeClient()
                .topApiMessageCorpConverSationAsyncSendV2(input);
        System.out.println(result.toJsonString());
    }

    @Test
    public void topapi_message_corpconversation_asyncsend_v2_markdown() throws Exception {
        TopApiMessageCorpConverSationAsyncSendV2Input input = new TopApiMessageCorpConverSationAsyncSendV2Input();
        input.setCmdId(RequestMappering.top_api_message_corp_conver_sation_async_send_v2.getMapperName());
        input.setAgentId(1338259958L);
        input.setUseridList("16393588697541721");
        NoticeMarkDown markDown = new NoticeMarkDown();
        markDown.setMsgtype(MsgtypeEnums.markdown);
        NoticeMarkDown.Markdown markdown = new NoticeMarkDown.Markdown();
        markdown.setTitle("首屏会话透出的展示内容");
        markdown.setText("# 这是支持markdown的文本   \\n   ## 标题2    \\n   * 列表1   \\n  ![alt 啊](https://img.alicdn.com/tps/TB1XLjqNVXXXXc4XVXXXXXXXXXX-170-64.png)");
        markDown.setMarkdown(markdown);
        input.setMsg(markDown);
        TopApiMessageCorpConverSationAsyncSendV2Result result = new DingTalkAPIClientBuilder()
                .setAccessToken("4a33dcc2bcc33d74995a53b174af5b09")
                .build()
                .getNoticeClient()
                .topApiMessageCorpConverSationAsyncSendV2(input);
        System.out.println(result.toJsonString());
    }

    @Test
    public void topapi_message_corpconversation_asyncsend_v2_action_card() throws Exception {
        TopApiMessageCorpConverSationAsyncSendV2Input input = new TopApiMessageCorpConverSationAsyncSendV2Input();
        input.setCmdId(RequestMappering.top_api_message_corp_conver_sation_async_send_v2.getMapperName());
        input.setAgentId(1338259958L);
        input.setUseridList("16393588697541721");
        NoticeActionCard noticeActionCard = new NoticeActionCard();
        noticeActionCard.setMsgtype(MsgtypeEnums.action_card);
        NoticeActionCard.ActionCard actionCard = new NoticeActionCard.ActionCard();
        actionCard.setTitle("是透出到会话列表和通知的文案");
        actionCard.setMarkdown("支持markdown格式的正文内容");
        actionCard.setSingle_title("查看详情");
        actionCard.setSingle_url("https://open.dingtalk.com");
        noticeActionCard.setAction_card(actionCard);
        input.setMsg(noticeActionCard);
        TopApiMessageCorpConverSationAsyncSendV2Result result = new DingTalkAPIClientBuilder()
                .setAccessToken("4a33dcc2bcc33d74995a53b174af5b09")
                .build()
                .getNoticeClient()
                .topApiMessageCorpConverSationAsyncSendV2(input);
        System.out.println(result.toJsonString());

    }
}
