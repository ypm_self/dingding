package com.jaka.framwork.core.dingding.addressbook;

import com.jaka.framework.core.dingding.DingTalkAPIClientBuilder;
import com.jaka.framework.core.dingding.api.addressbook.dept.input.*;
import com.jaka.framework.core.dingding.api.addressbook.dept.result.*;
import com.jaka.framework.core.dingding.api.addressbook.role.input.TopApiRoleListInput;
import com.jaka.framework.core.dingding.api.addressbook.role.result.TopApiRoleListResult;
import com.jaka.framework.core.dingding.api.addressbook.user.input.TopApiUserCountInput;
import com.jaka.framework.core.dingding.api.addressbook.user.input.TopApiV2UserGetInput;
import com.jaka.framework.core.dingding.api.addressbook.user.input.TopApinactiveUserV2GetInput;
import com.jaka.framework.core.dingding.api.addressbook.user.result.TopApiUserCountResult;
import com.jaka.framework.core.dingding.api.addressbook.user.result.TopApiV2UserGetResult;
import com.jaka.framework.core.dingding.api.addressbook.user.result.TopApinactiveUserV2GetResult;
import com.jaka.framework.core.dingding.api.user.input.TopApiUserListid;
import com.jaka.framework.core.dingding.api.user.result.TopApiUserListIdResult;
import com.jaka.framework.core.dingding.base.en.RequestMappering;
import org.junit.Test;

/**
 * @author ：james.liu
 * @date ：Created in 2021/12/27 10:56
 * @description：
 * @version: 1.0
 */
public class TestAddressBook {
    
    @Test
    public void test_address_book_role_list() throws Exception {
        TopApiRoleListInput input = new TopApiRoleListInput();
        input.setCmdId(RequestMappering.topapi_role_list.getMapperName());
        TopApiRoleListResult result = new DingTalkAPIClientBuilder()
                .setAccessToken("804443609beb3aee8753cc0f6537a21a")
                .build().getAddressBookClient().topApiRoleList(input);
        System.out.println(result.toJsonString());
    }
    
    /**
     * @throws Exception
     */
    @Test
    public void test_dingding_get_department_list() throws Exception {
        TopapiV2DepartmentListsubidInput input = new TopapiV2DepartmentListsubidInput();
        input.setDeptId(60659996L);
        input.setCmdId(RequestMappering.get_department_list.getMapperName());
        TopapiV2DepartmentListsubidResult departmentList = new DingTalkAPIClientBuilder()
                .setAccessToken("1667706029173ea3a4c8e0e4f145c519")
                .build()
                .getAddressBookClient()
                .getDepartmentList(input);
        //{"deptIdList":[60659996,118113545,445398703,5953950,396147611,102645113,69641454,60712999,494265132,540907763,445931387]}}
        System.out.println(departmentList.toJsonString());
    }
    
    @Test
    public void test_dingding_top_api_v2_department_list_sub() throws Exception {
        TopapiV2DepartmentListsubInput input = new TopapiV2DepartmentListsubInput();
        input.setCmdId(RequestMappering.topapi_v2_department_listsub.getMapperName());
        TopapiV2DepartmentListsubResult result = new DingTalkAPIClientBuilder()
                .setAccessToken("1667706029173ea3a4c8e0e4f145c519")
                .build()
                .getAddressBookClient()
                .topapiV2DepartmentListsub(input);
        System.out.println(result.toJsonString());
    }
    
    /**
     * 获取部门详情
     *
     * @throws Exception
     */
    @Test
    public void test_dingding_get_department() throws Exception {
        TopapiV2DepartmentGetInput input = new TopapiV2DepartmentGetInput();
        //{"deptIdList":[60659996,118113545,445398703,5953950,396147611,102645113,69641454,60712999,494265132,540907763,445931387]}}
        //:{"deptIdList":[505231885,445083506,445473538]}}
        input.setDeptId(445931387L);
        input.setCmdId(RequestMappering.get_department.getMapperName());
        TopapiV2DepartmentGetResult department =
                new DingTalkAPIClientBuilder()
                        .setAccessToken("d7fc5d826e273e2992b1fce8ce55e666")
                        .build()
                        .getAddressBookClient()
                        .getDepartment(input);
        System.out.println(department.toJsonString());
        
    }
    
    @Test
    public void test_dingdind_topapi_v2_department_listparentbydept() throws Exception {
        TopapiV2DepartmentListParentByDeptInput input = new TopapiV2DepartmentListParentByDeptInput();
        input.setCmdId(RequestMappering.topapi_v2_department_listparentbydept.getMapperName());
        input.setDeptId(445931387L);
        TopapiV2DepartmentListParentByDeptResult result = new DingTalkAPIClientBuilder()
                .setAccessToken("1667706029173ea3a4c8e0e4f145c519")
                .build()
                .getAddressBookClient()
                .topapiV2DepartmentListParentByDept(input);
        System.out.println(result.toJsonString());
    }
    
    @Test
    public void test_dingding_topapi_v2_department_listparentbydept() throws Exception {
        TopApiV2DepartmentListParentByuUerInput input = new TopApiV2DepartmentListParentByuUerInput();
        input.setCmdId(RequestMappering.topapi_v2_department_list_parentbyuser.getMapperName());
        input.setUserid(16393588697541721L);
        TopApiV2DepartmentListParentByuUerResult result = new DingTalkAPIClientBuilder()
                .setAccessToken("1667706029173ea3a4c8e0e4f145c519")
                .build()
                .getAddressBookClient()
                .topApiV2DepartmentListParentByuUer(input);
        System.out.println(result.toJsonString());
    }
    
    @Test
    public void test_dingding_get_uset_listid() throws Exception {
        TopApiUserListid input = new TopApiUserListid();
        input.setCmdId(RequestMappering.get_uset_listid.getMapperName());
        input.setDeptId(5953950L);
        TopApiUserListIdResult apiUserListIdResult = new DingTalkAPIClientBuilder()
                .setAccessToken("96386c7b3b1c3c5b93a4bf276a61a8f9")
                .build()
                .getAddressBookClient()
                .getUserListId(input);
        System.out.println(apiUserListIdResult.toJsonString());
        //{"requestId":"6ncn1xf2l84n","errcode":0,"errmsg":"ok","success":false,"result":{"useridList":["16383203545014475","040448261529513686","1267345821953140","175766313627602720","142123253920035735","144822394137615559","16357274550104888","16383198797412473","066335132035886933","225737441833091897"]}}
    }
    
    /**
     * @throws Exception
     */
    @Test
    public void test_dingding_get_user() throws Exception {
        TopApiV2UserGetInput input = new TopApiV2UserGetInput();
        input.setUserid("16393588697541721");
        input.setCmdId(RequestMappering.topapi_v2_user_get.getMapperName());
        TopApiV2UserGetResult user = new DingTalkAPIClientBuilder().setAccessToken("96386c7b3b1c3c5b93a4bf276a61a8f9").build().getAddressBookClient().topApiV2UserGet(input);
        System.out.println("---->>> : " + user.toJsonString());
    }
    
    @Test
    public void test_dingding_topapi_user_count() throws Exception {
        TopApiUserCountInput input = new TopApiUserCountInput();
        input.setCmdId(RequestMappering.topapi_user_count.getMapperName());
        TopApiUserCountResult result = new DingTalkAPIClientBuilder()
                .setAccessToken("570e00483b3c32f9b73b564436fcd67e")
                .build().getAddressBookClient().topApiUserCount(input);
        System.out.println(result.toJsonString());
        
    }
    
    @Test
    public void test_dingding_topapi_inactive_user_v2_get() throws Exception {
        TopApinactiveUserV2GetInput input = new TopApinactiveUserV2GetInput();
        input.setCmdId(RequestMappering.topapi_inactive_user_v2_get.getMapperName());
//        input.setActive(true);
        TopApinactiveUserV2GetResult result = new DingTalkAPIClientBuilder()
                .setAccessToken("570e00483b3c32f9b73b564436fcd67e")
                .build().getAddressBookClient()
                .topApinactiveUserV2Get(input);
        System.out.println(result.toJsonString());
    }
    
}
