package com.jaka.framwork.core.dingding.hrm;

import com.jaka.framework.core.dingding.DingTalkAPIClientBuilder;
import com.jaka.framework.core.dingding.api.hrm.input.SmartWorkHrmEmployeeListDimissionInput;
import com.jaka.framework.core.dingding.api.hrm.input.SmartWorkHrmEmployeeQueryDimissionInput;
import com.jaka.framework.core.dingding.api.hrm.input.SmartWorkHrmEmployeeQueryPreentryInput;
import com.jaka.framework.core.dingding.api.hrm.input.SmartWorkHrmEmployeeQueryonJobInput;
import com.jaka.framework.core.dingding.api.hrm.result.SmartWorkHrmEmployeeListDimissionResult;
import com.jaka.framework.core.dingding.api.hrm.result.SmartWorkHrmEmployeeQueryDimissionResult;
import com.jaka.framework.core.dingding.api.hrm.result.SmartWorkHrmEmployeeQueryPreentryResult;
import com.jaka.framework.core.dingding.api.hrm.result.SmartWorkHrmEmployeeQueryonJobResult;
import com.jaka.framework.core.dingding.base.en.RequestMappering;
import org.junit.Test;

/**
 * @author ：james.liu
 * @date ：Created in 2021/12/24 10:29
 * @description：只能人事测试
 * @version: 1.0
 */
public class TestDDHRM {

    /**
     * 获取在职员工列表获取在职员工列表
     *
     * @throws Exception
     */
    @Test
    public void test_dd_hrm_employee_queryon_job() throws Exception {
        SmartWorkHrmEmployeeQueryonJobInput input = new SmartWorkHrmEmployeeQueryonJobInput();
        input.setStatusList("2,3,5,-1");
        input.setOffset(350L);
        input.setCmdId(RequestMappering.get_employee_queryonjob.getMapperName());
        SmartWorkHrmEmployeeQueryonJobResult result = new DingTalkAPIClientBuilder()
                .setAccessToken("d7fc5d826e273e2992b1fce8ce55e666")
                .build()
                .getSmartWorkHrmClient()
                .hrmEmployeeQueryonJob(input);
        String dataList = result.getResult().getDataList();
        System.out.println(dataList.split(",").length);
        System.out.println(result.toJsonString());
    }

    /**
     * 获取待入职员工列表
     *
     * @throws Exception
     */
    @Test
    public void test_smart_work_hrm_empployee_query_preentry() throws Exception {
        SmartWorkHrmEmployeeQueryPreentryInput input = new SmartWorkHrmEmployeeQueryPreentryInput();
        input.setCmdId(RequestMappering.hrm_employee_query_preentry.getMapperName());
        SmartWorkHrmEmployeeQueryPreentryResult result = new DingTalkAPIClientBuilder()
                .setAccessToken("e72e86cafb0f3dd385b8e9ecf40a03a7")
                .build()
                .getSmartWorkHrmClient()
                .hrmEmployeeQueryPreentry(input);
        System.out.println(result.toJsonString());
    }

    /**
     * 获取离职员工列表
     */
    @Test
    public void test_smart_work_hrm_empployee_query_dimission() throws Exception {
        SmartWorkHrmEmployeeQueryDimissionInput input = new SmartWorkHrmEmployeeQueryDimissionInput();
        input.setCmdId(RequestMappering.hrm_employee_query_dimission.getMapperName());
        SmartWorkHrmEmployeeQueryDimissionResult result = new DingTalkAPIClientBuilder()
                .setAccessToken("d7fc5d826e273e2992b1fce8ce55e666")
                .build()
                .getSmartWorkHrmClient()
                .hrmEmployeeQueryDimission(input);
        System.out.println(result.toJsonString());
    }

    /**
     * 获取员工离职信息
     *
     * @throws Exception
     */
    @Test
    public void test_smart_work_hrm_empployee_list_dimission() throws Exception {
        SmartWorkHrmEmployeeListDimissionInput input = new SmartWorkHrmEmployeeListDimissionInput();
        input.setCmdId(RequestMappering.hrm_employee_list_dimission.getMapperName());
        input.setUserid_list("");
        SmartWorkHrmEmployeeListDimissionResult result = new DingTalkAPIClientBuilder()
                .setAccessToken("e72e86cafb0f3dd385b8e9ecf40a03a7")
                .build()
                .getSmartWorkHrmClient()
                .smartWorkHrmEmployeeListDimission(input);
    }
}
