package com.jaka.framework.core.dingding.api.callback;

/**
 * @author ：james.liu
 * @date ：Created in 2021/12/30 10:48
 * @description：
 * @version: 1.0
 */
public class CallBackUserBaseResult extends CallBackBaseResult {
    /**
     * 发生通讯录变更的企业
     */
    private String CorpId;
    /**
     * 时间戳。v
     */
    private String TimeStamp;

    public String getCorpId() {
        return CorpId;
    }

    public void setCorpId(String corpId) {
        CorpId = corpId;
    }

    public String getTimeStamp() {
        return TimeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        TimeStamp = timeStamp;
    }
}
