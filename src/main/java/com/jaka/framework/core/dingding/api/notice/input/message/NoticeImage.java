package com.jaka.framework.core.dingding.api.notice.input.message;

/**
 * @author ：james.liu
 * @date ：Created in 2021/12/29 10:24
 * @description：钉钉消息发送图片类型消息
 * @version: 1.0
 */
public class NoticeImage extends NoticeBaseModel {

    /**
     *
     */
    private Image image;

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }

    public static class Image {
        /**
         * 媒体文件mediaid，建议宽600像素 x 400像素，宽高比3 : 2。
         */
        private String media_id;

        public String getMedia_id() {
            return media_id;
        }

        public void setMedia_id(String media_id) {
            this.media_id = media_id;
        }
    }

}
