package com.jaka.framework.core.dingding.em;

/**
 * @author ：james.liu
 * @date ：Created in 2021/12/24 10:58
 * @description：
 * @version: 1.0
 */
public enum ReasonTypeEnums {

    reason_type_1(1, "家庭原因"),
    reason_type_2(2, "个人原因"),
    reason_type_3(3, "发展原因"),
    reason_type_4(4, "合同到期不续签"),
    reason_type_5(5, "协议解除"),
    reason_type_6(6, "无法胜任工作"),
    reason_type_7(7, "经济性裁员"),
    reason_type_8(8, "严重违法违纪"),
    reason_type_9(9, "其他"),;

    ReasonTypeEnums(Integer type, String desc) {
        this.type = type;
        this.desc = desc;
    }

    /**
     * 类型编号
     */
    private Integer type;
    /**
     * 解释
     */
    private String desc;

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
