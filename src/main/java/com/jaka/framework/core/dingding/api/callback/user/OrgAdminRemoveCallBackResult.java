package com.jaka.framework.core.dingding.api.callback.user;

import com.jaka.framework.core.dingding.api.callback.CallBackUserBaseResult;

import java.util.List;

/**
 * @author ：james.liu
 * @date ：Created in 2021/12/30 11:05
 * @description：通讯录事件-通讯录用户被取消设置管理员
 * @version: 1.0
 */
public class OrgAdminRemoveCallBackResult extends CallBackUserBaseResult {

    /**
     * 用户发生变更的userId列表。
     */
    private List<String> UserId;

    public List<String> getUserId() {
        return UserId;
    }

    public void setUserId(List<String> userId) {
        UserId = userId;
    }
}
