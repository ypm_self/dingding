package com.jaka.framework.core.dingding.api.callback.user;

import com.jaka.framework.core.dingding.api.callback.CallBackUserBaseResult;

import java.util.List;

/**
 * @author ：james.liu
 * @date ：Created in 2021/12/30 11:04
 * @description：通讯录事件-通讯录用户被设为管理员
 * @version: 1.0
 */
public class OrgAdminAddCallBackResult extends CallBackUserBaseResult {

    /**
     * 用户发生变更的userId列表。
     */
    private List<String> UserId;

    public List<String> getUserId() {
        return UserId;
    }

    public void setUserId(List<String> userId) {
        UserId = userId;
    }
}
