package com.jaka.framework.core.dingding.api.file.result;

import com.jaka.framework.core.dingding.base.AbstractAPIResult;

/**
 * @author ：james.liu
 * @date ：Created in 2021/12/29 10:45
 * @description：上传媒体文件
 * @version: 1.0
 */
public class MediaUploadResult extends AbstractAPIResult {
    /**
     * 媒体文件类型：
     * image：图片
     * voice：语音
     * file：普通文件
     * video：视频
     */
    private String type;
    /**
     * 媒体文件上传后获取的唯一标识。
     */
    private String media_id;
    /**
     * 媒体文件上传时间戳。
     */
    private Long created_at;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMedia_id() {
        return media_id;
    }

    public void setMedia_id(String media_id) {
        this.media_id = media_id;
    }

    public Long getCreated_at() {
        return created_at;
    }

    public void setCreated_at(Long created_at) {
        this.created_at = created_at;
    }
}
