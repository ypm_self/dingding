package com.jaka.framework.core.dingding.api.addressbook.dept.result;

import com.jaka.framework.core.dingding.base.AbstractAPIResult;

import java.util.List;

/**
 * @author ：james.liu
 * @date ：Created in 2021/12/27 16:13
 * @description：获取指定部门的所有父部门列表
 * @version: 1.0
 */
public class TopapiV2DepartmentListParentByDeptResult extends AbstractAPIResult {
    /**
     * 父部门ID列表。
     */
    private DeptListParentByDeptIdResponse result;

    public DeptListParentByDeptIdResponse getResult() {
        return result;
    }

    public void setResult(DeptListParentByDeptIdResponse result) {
        this.result = result;
    }

    public static class  DeptListParentByDeptIdResponse{
        /**
         * 该部门的所有父部门ID列表。
         */
        private List<Long> parent_id_list;

        public List<Long> getParent_id_list() {
            return parent_id_list;
        }

        public void setParent_id_list(List<Long> parent_id_list) {
            this.parent_id_list = parent_id_list;
        }
    }
}
