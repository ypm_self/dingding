package com.jaka.framework.core.dingding.api.callback.attendance;


import com.jaka.framework.core.dingding.api.callback.CallBackBaseResult;

import java.util.List;

/**
 * @author ：james.liu
 * @date ：Created in 2021/12/30 9:22
 * @description：考勤事件-员工打卡事件
 * @version: 1.0
 */
public class AttendanceCheckRecordCallBackResult extends CallBackBaseResult {

    private List<AttendanceCheckRecordDataList> DataList;

    public List<AttendanceCheckRecordDataList> getDataList() {
        return DataList;
    }

    public void setDataList(List<AttendanceCheckRecordDataList> dataList) {
        DataList = dataList;
    }

    public static class AttendanceCheckRecordDataList {
        /**
         * 打卡位置。
         */
        private String address;
        /**
         * 打卡时间。
         */
        private Long checkTime;
        /**
         * 企业的corpid。
         */
        private String corpId;
        /**
         * 考勤组的groupId。
         */
        private String groupId;
        /**
         * 纬度信息。
         */
        private Float latitude;
        /**
         * 关联的业务ID。
         */
        private String bizId;
        /**
         * 打卡方式。
         * MAP：定位打卡
         * WIFI：wifi打卡
         * ATM：考勤机打卡或考勤机蓝牙打卡
         */
        private String locationMethod;
        /**
         * 员工的userId。
         */
        private String userId;
        /**
         * 经度信息。
         */
        private String deviceSN;
        /**
         * 考勤机SN。
         * 当打卡方式为考勤机打卡时返回此字段。
         */
        private Float longitude;

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public Long getCheckTime() {
            return checkTime;
        }

        public void setCheckTime(Long checkTime) {
            this.checkTime = checkTime;
        }

        public String getCorpId() {
            return corpId;
        }

        public void setCorpId(String corpId) {
            this.corpId = corpId;
        }

        public String getGroupId() {
            return groupId;
        }

        public void setGroupId(String groupId) {
            this.groupId = groupId;
        }

        public Float getLatitude() {
            return latitude;
        }

        public void setLatitude(Float latitude) {
            this.latitude = latitude;
        }

        public String getBizId() {
            return bizId;
        }

        public void setBizId(String bizId) {
            this.bizId = bizId;
        }

        public String getLocationMethod() {
            return locationMethod;
        }

        public void setLocationMethod(String locationMethod) {
            this.locationMethod = locationMethod;
        }

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public String getDeviceSN() {
            return deviceSN;
        }

        public void setDeviceSN(String deviceSN) {
            this.deviceSN = deviceSN;
        }

        public Float getLongitude() {
            return longitude;
        }

        public void setLongitude(Float longitude) {
            this.longitude = longitude;
        }
    }
}
