package com.jaka.framework.core.dingding.api.addressbook.role.input;

import com.jaka.framework.core.dingding.base.AbstractAPIInput;
import okhttp3.FormBody;
import okhttp3.RequestBody;

/**
 * @author ：james.liu
 * @date ：Created in 2021/12/27 10:50
 * @description：获取角色列表
 * @version: 1.0
 */
public class TopApiRoleListInput extends AbstractAPIInput {

    /**
     * 支持分页查询，与offset参数同时设置时才生效，此参数代表分页大小，默认值20，最大值200。
     */
    private Long size = 20L;
    /**
     * 支持分页查询，与size参数同时设置时才生效，此参数代表偏移量，偏移量从0开始。
     */
    private Long offset = 0L;

    public Long getSize() {
        return size;
    }

    public void setSize(Long size) {
        this.size = size;
    }

    public Long getOffset() {
        return offset;
    }

    public void setOffset(Long offset) {
        this.offset = offset;
    }


    /**
     * 描述：转换RequestBody对象
     */
    public final RequestBody toRequestBody() {
        RequestBody r = null;
        final FormBody.Builder builder = new FormBody.Builder();
        builder.add("size", this.getSize().toString());
        builder.add("offset", this.getOffset().toString());
        r = builder.build();
        return r;
    }
}
