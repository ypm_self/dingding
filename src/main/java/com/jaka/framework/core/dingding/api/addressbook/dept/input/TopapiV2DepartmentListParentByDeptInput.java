package com.jaka.framework.core.dingding.api.addressbook.dept.input;

import com.jaka.framework.core.dingding.base.AbstractAPIInput;
import okhttp3.FormBody;
import okhttp3.RequestBody;

/**
 * @author ：james.liu
 * @date ：Created in 2021/12/27 16:12
 * @description：获取指定部门的所有父部门列表
 * @version: 1.0
 */
public class TopapiV2DepartmentListParentByDeptInput extends AbstractAPIInput {

    private Long deptId;

    public Long getDeptId() {
        return deptId;
    }

    public void setDeptId(Long deptId) {
        this.deptId = deptId;
    }


    /**
     * 描述：转换RequestBody对象
     */
    public final RequestBody toRequestBody() {
        RequestBody r = null;
        final FormBody.Builder builder = new FormBody.Builder();
        builder.add("dept_id", this.getDeptId().toString());
        r = builder.build();
        return r;
    }
}
