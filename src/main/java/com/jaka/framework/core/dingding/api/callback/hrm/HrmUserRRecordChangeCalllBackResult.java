package com.jaka.framework.core.dingding.api.callback.hrm;

import com.jaka.framework.core.dingding.api.callback.CallBackBaseResult;

/**
 * @author ：james.liu
 * @date ：Created in 2021/12/30 10:34
 * @description：智能人事事件
 * @version: 1.0
 */
public class HrmUserRRecordChangeCalllBackResult extends CallBackBaseResult {
    /**
     * 触发事件的动作类型。
     */
    private String actionType;
    /**
     * 发生人事变更的员工的ID。
     */
    private String staffId;

    public String getActionType() {
        return actionType;
    }

    public void setActionType(String actionType) {
        this.actionType = actionType;
    }

    public String getStaffId() {
        return staffId;
    }

    public void setStaffId(String staffId) {
        this.staffId = staffId;
    }
}
