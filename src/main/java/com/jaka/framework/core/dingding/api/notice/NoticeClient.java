package com.jaka.framework.core.dingding.api.notice;

import com.jaka.framework.common.core.base.IHttpUtils;
import com.jaka.framework.core.dingding.api.notice.input.*;
import com.jaka.framework.core.dingding.api.notice.result.*;
import com.jaka.framework.core.dingding.base.DefaultAPIActionProxy;
import com.jaka.framework.core.dingding.base.en.RequestMappering;
import com.jaka.framework.core.dingding.config.APIClientConfiguration;
import org.apache.commons.lang3.StringUtils;

/**
 * @author ：james.liu
 * @date ：Created in 2021/12/29 9:34
 * @description：钉钉消息通知
 * @version: 1.0
 */
public class NoticeClient {


    private final IHttpUtils httpUtils;
    private final APIClientConfiguration configuration;

    public NoticeClient(final APIClientConfiguration configuration, final IHttpUtils httpUtils) {
        this.configuration = configuration;
        this.httpUtils = httpUtils;
    }

    /**
     * 发送工作通知
     *
     * @param input
     * @return
     * @throws Exception
     */
    public TopApiMessageCorpConverSationAsyncSendV2Result topApiMessageCorpConverSationAsyncSendV2(
            final TopApiMessageCorpConverSationAsyncSendV2Input input) throws Exception {
        if(StringUtils.isBlank(input.getCmdId())){
            input.setCmdId(RequestMappering.top_api_message_corp_conver_sation_async_send_v2.getMapperName());
        }
        return DefaultAPIActionProxy.getInstance().doAction(input,
                configuration,
                httpUtils,
                TopApiMessageCorpConverSationAsyncSendV2Result.class,
                input.toRequestBody());
    }

    /**
     * 更新工作通知状态栏
     *
     * @param input
     * @return
     * @throws Exception
     */
    public TopApiMessageCorpConversAtionStatusBarUpdateResult topApiMessageCorpConversAtionStatusBarUpdate(
            final TopApiMessageCorpConversAtionStatusBarUpdateInput input) throws Exception {
        return DefaultAPIActionProxy.getInstance().doAction(input,
                configuration,
                httpUtils,
                TopApiMessageCorpConversAtionStatusBarUpdateResult.class,
                input.toRequestBody());
    }

    /**
     * 获取工作通知消息的发送进度
     *
     * @param input
     * @return
     * @throws Exception
     */
    public TopApiMessageCorpConverSationGetsendprogressResult topApiMessageCorpConverSationGetsendprogress(
            final TopApiMessageCorpConverSationGetsendprogressInput input) throws Exception {
        return DefaultAPIActionProxy.getInstance().doAction(input,
                configuration,
                httpUtils,
                TopApiMessageCorpConverSationGetsendprogressResult.class,
                input.toRequestBody());
    }

    /**
     * 获取工作通知消息的发送结果
     *
     * @param input
     * @return
     * @throws Exception
     */
    public TopapiMessageCorpConverSationGetSendResult topapiMessageCorpConverSationGetSend(
            final TopapiMessageCorpConverSationGetSendResultInput input) throws Exception {
        return DefaultAPIActionProxy.getInstance().doAction(input,
                configuration,
                httpUtils,
                TopapiMessageCorpConverSationGetSendResult.class,
                input.toRequestBody());
    }

    /**
     * 撤回工作通知消息
     *
     * @param input
     * @return
     * @throws Exception
     */
    public TopApiMessageCorpConverSationRecallResult topApiMessageCorpConverSationRecall(
            final TopApiMessageCorpConverSationRecallInput input) throws Exception {
        return DefaultAPIActionProxy.getInstance().doAction(input,
                configuration,
                httpUtils,
                TopApiMessageCorpConverSationRecallResult.class,
                input.toRequestBody());
    }

    /**
     * 发送消息到企业群
     *
     * @param input
     * @return
     * @throws Exception
     */
    public ChatSendResult chatSend(final ChatSendInput input) throws Exception {
        return DefaultAPIActionProxy.getInstance().doAction(input,
                configuration,
                httpUtils,
                ChatSendResult.class,
                input.toRequestBody());
    }


}
