package com.jaka.framework.core.dingding.api.notice.input;

import com.jaka.framework.core.dingding.base.AbstractAPIInput;
import okhttp3.FormBody;
import okhttp3.RequestBody;

/**
 * @author ：james.liu
 * @date ：Created in 2021/12/29 15:22
 * @description：撤回工作通知消息
 * @version: 1.0
 */
public class TopApiMessageCorpConverSationRecallInput extends AbstractAPIInput {
    /**
     * 发送消息时使用的微应用的AgentID。
     * 企业内部应用可在开发者后台的应用详情页面查看。
     * 第三方企业应用可调用获取企业授权信息接口获取。
     */
    private Long agentId;
    /**
     * 发送消息时钉钉返回的任务ID。
     */
    private Long msgTaskId;

    public Long getAgentId() {
        return agentId;
    }

    public void setAgentId(Long agentId) {
        this.agentId = agentId;
    }

    public Long getMsgTaskId() {
        return msgTaskId;
    }

    public void setMsgTaskId(Long msgTaskId) {
        this.msgTaskId = msgTaskId;
    }

    /**
     * 描述：转换RequestBody对象
     */
    public final RequestBody toRequestBody() {
        final FormBody.Builder builder = new FormBody.Builder();
        builder.add("msg_task_id", this.getAgentId().toString());
        builder.add("msg_task_id", this.getMsgTaskId().toString());
        RequestBody r = builder.build();
        return r;
    }
}
