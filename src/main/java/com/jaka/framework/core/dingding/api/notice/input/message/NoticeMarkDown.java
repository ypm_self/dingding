package com.jaka.framework.core.dingding.api.notice.input.message;

/**
 * @author ：james.liu
 * @date ：Created in 2021/12/29 14:18
 * @description：markdown消息
 * @version: 1.0
 */
public class NoticeMarkDown extends NoticeBaseModel {

    private Markdown markdown;

    public Markdown getMarkdown() {
        return markdown;
    }

    public void setMarkdown(Markdown markdown) {
        this.markdown = markdown;
    }

    public static class Markdown {
        /**
         * 首屏会话透出的展示内容。
         */
        private String title;
        /**
         * markdown格式的消息，最大不超过5000字符。
         */
        private String text;

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }
    }
}
