package com.jaka.framework.core.dingding.api.hrm.result;

import com.jaka.framework.core.dingding.base.AbstractAPIResult;

import java.util.List;

/**
 * @author ：james.liu
 * @date ：Created in 2021/12/24 10:53
 * @description：获取员工离职信息出参
 * @version: 1.0
 */
public class SmartWorkHrmEmployeeListDimissionResult extends AbstractAPIResult {
    /**
     * 返回结果。
     */
    private EmpDimissionInfoVo result;

    public EmpDimissionInfoVo getResult() {
        return result;
    }

    public void setResult(EmpDimissionInfoVo result) {
        this.result = result;
    }

    public static class EmpDimissionInfoVo {
        /**
         * 离职员工的userid。
         */
        private String userid;
        /**
         * 最后工作日。
         */
        private Long last_work_day;
        /**
         * 离职部门列表。
         */
        private List<EmpDeptVO> dept_list;
        /**
         * 离职原因备注
         */
        private String reason_memo;
        /**
         * 离职原因类型：
         * 1：家庭原因
         * 2：个人原因
         * 3：发展原因
         * 4：合同到期不续签
         * 5：协议解除
         * 6：无法胜任工作
         * 7：经济性裁员
         * 8：严重违法违纪
         * 9：其他
         */
        private Integer reason_type;
        /**
         * 离职前工作状态：
         * 1：待入职
         * 2：试用期
         * 3：正式
         */
        private Integer pre_status;
        /**
         * 离职交接人的userid。
         */
        private String handover_userid;
        /**
         * 离职状态：
         * 1：待离职
         * 2：已离职
         * 3：未离职
         * 4：发起离职审批但还未通过
         * 5：失效（离职流程被其他流程强制终止后的状态）
         */
        private Integer status;
        /**
         * 离职前主部门名称。
         */
        private String main_dept_name;
        /**
         * 离职前主部门ID。
         */
        private Long main_dept_id;

        public String getUserid() {
            return userid;
        }

        public void setUserid(String userid) {
            this.userid = userid;
        }

        public Long getLast_work_day() {
            return last_work_day;
        }

        public void setLast_work_day(Long last_work_day) {
            this.last_work_day = last_work_day;
        }

        public List<EmpDeptVO> getDept_list() {
            return dept_list;
        }

        public void setDept_list(List<EmpDeptVO> dept_list) {
            this.dept_list = dept_list;
        }

        public String getReason_memo() {
            return reason_memo;
        }

        public void setReason_memo(String reason_memo) {
            this.reason_memo = reason_memo;
        }

        public Integer getReason_type() {
            return reason_type;
        }

        public void setReason_type(Integer reason_type) {
            this.reason_type = reason_type;
        }

        public Integer getPre_status() {
            return pre_status;
        }

        public void setPre_status(Integer pre_status) {
            this.pre_status = pre_status;
        }

        public String getHandover_userid() {
            return handover_userid;
        }

        public void setHandover_userid(String handover_userid) {
            this.handover_userid = handover_userid;
        }

        public Integer getStatus() {
            return status;
        }

        public void setStatus(Integer status) {
            this.status = status;
        }

        public String getMain_dept_name() {
            return main_dept_name;
        }

        public void setMain_dept_name(String main_dept_name) {
            this.main_dept_name = main_dept_name;
        }

        public Long getMain_dept_id() {
            return main_dept_id;
        }

        public void setMain_dept_id(Long main_dept_id) {
            this.main_dept_id = main_dept_id;
        }
    }


    public static class EmpDeptVO {
        /**
         * 部门名称。
         */
        private String dept_path;
        /**
         * 部门ID。
         */
        private Long dept_id;

        public String getDept_path() {
            return dept_path;
        }

        public void setDept_path(String dept_path) {
            this.dept_path = dept_path;
        }

        public Long getDept_id() {
            return dept_id;
        }

        public void setDept_id(Long dept_id) {
            this.dept_id = dept_id;
        }
    }
}
