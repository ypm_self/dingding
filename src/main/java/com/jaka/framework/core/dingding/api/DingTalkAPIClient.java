package com.jaka.framework.core.dingding.api;


import com.jaka.framework.common.core.base.IHttpUtils;
import com.jaka.framework.core.dingding.api.addressbook.AddressBookClient;
import com.jaka.framework.core.dingding.api.auth.AuthClient;
import com.jaka.framework.core.dingding.api.file.FileClient;
import com.jaka.framework.core.dingding.api.hrm.SmartWorkHrmClient;
import com.jaka.framework.core.dingding.api.notice.NoticeClient;
import com.jaka.framework.core.dingding.api.user.UserClient;
import com.jaka.framework.core.dingding.config.APIClientConfiguration;

/**
 * @author ：james.liu
 * @date ：Created in 2021/12/14 17:49
 * @description：API客户端
 * @version: 1.0
 */
public final class DingTalkAPIClient {
    /*配置中心参数*/
    private final APIClientConfiguration configuration;
    
    private final IHttpUtils httpUtils;
    /*用户相关客户端*/
    private final UserClient userClient;
    /*推送相关客户端*/
    private final NoticeClient noticeClient;
    /*智能人事相关客户端*/
    private final SmartWorkHrmClient smartWorkHrmClient;
    /*通讯录管理客户端*/
    private final AddressBookClient addressBookClient;
    /*文件存储客户端*/
    private final FileClient fileClient;
//    /*认证相关接口*/
    private final AuthClient authClient;
    
    public DingTalkAPIClient(final APIClientConfiguration configuration, final IHttpUtils httpUtils) {
        this.configuration = configuration;
        this.httpUtils = httpUtils;
        this.userClient = new UserClient(configuration, httpUtils);
        this.noticeClient = new NoticeClient(configuration, httpUtils);
        this.smartWorkHrmClient = new SmartWorkHrmClient(configuration, httpUtils);
        this.addressBookClient = new AddressBookClient(configuration, httpUtils);
        this.fileClient = new FileClient(configuration, httpUtils);
        this.authClient = new AuthClient(configuration, httpUtils);
    }
    
    public APIClientConfiguration getConfiguration() {
        return configuration;
    }
    
    public IHttpUtils getHttpUtils() {
        return httpUtils;
    }
    
    public UserClient getUserClient() {
        return userClient;
    }
    
    public NoticeClient getNoticeClient() {
        return noticeClient;
    }
    
    public SmartWorkHrmClient getSmartWorkHrmClient() {
        return smartWorkHrmClient;
    }
    
    public AddressBookClient getAddressBookClient() {
        return addressBookClient;
    }
    
    public FileClient getFileClient() {
        return fileClient;
    }

    public AuthClient getAuthClient() {
        return authClient;
    }
}
