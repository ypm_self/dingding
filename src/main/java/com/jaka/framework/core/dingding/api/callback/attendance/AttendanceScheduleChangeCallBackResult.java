package com.jaka.framework.core.dingding.api.callback.attendance;


import com.jaka.framework.core.dingding.api.callback.CallBackBaseResult;

import java.util.List;

/**
 * @author ：james.liu
 * @date ：Created in 2021/12/30 9:32
 * @description：考勤事件-员工排班变更事件
 * @version: 1.0
 */
public class AttendanceScheduleChangeCallBackResult extends CallBackBaseResult {
    /**
     *
     */
    private AttendanceScheduleChangeDataList DataList;

    public AttendanceScheduleChangeDataList getDataList() {
        return DataList;
    }

    public void setDataList(AttendanceScheduleChangeDataList dataList) {
        DataList = dataList;
    }

    public static class AttendanceScheduleChangeDataList {
        /**
         * 排班结束时间。
         */
        private Long workDateEnd;
        /**
         * 企业的corpId。
         */
        private String corpId;
        /**
         * 类型。
         * modify：修改排班
         * delete：删除排班
         */
        private String type;
        /**
         * 排班开始时间。
         */
        private Long workDateBegin;
        /**
         * 员工的userIds列表。s
         */
        private List<String> userIds;

        public Long getWorkDateEnd() {
            return workDateEnd;
        }

        public void setWorkDateEnd(Long workDateEnd) {
            this.workDateEnd = workDateEnd;
        }

        public String getCorpId() {
            return corpId;
        }

        public void setCorpId(String corpId) {
            this.corpId = corpId;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public Long getWorkDateBegin() {
            return workDateBegin;
        }

        public void setWorkDateBegin(Long workDateBegin) {
            this.workDateBegin = workDateBegin;
        }

        public List<String> getUserIds() {
            return userIds;
        }

        public void setUserIds(List<String> userIds) {
            this.userIds = userIds;
        }
    }
}
