package com.jaka.framework.core.dingding.api.notice.result;

import com.jaka.framework.core.dingding.base.AbstractAPIResult;

/**
 * @author ：james.liu
 * @date ：Created in 2021/12/29 15:09
 * @description：获取工作通知消息的发送进度
 * @version: 1.0
 */
public class TopApiMessageCorpConverSationGetsendprogressResult extends AbstractAPIResult {

    /**
     * 返回结果。
     */
    private AsyncSendProgress progress;

    public AsyncSendProgress getProgress() {
        return progress;
    }

    public void setProgress(AsyncSendProgress progress) {
        this.progress = progress;
    }

    public static class AsyncSendProgress {
        /**
         * 取值0~100，表示处理的百分比。
         */
        private Long progress_in_percent;
        /**
         * 任务执行状态：
         * 0：未开始
         * 1：处理中
         * 2：处理完毕
         */
        private Long status;

        public Long getProgress_in_percent() {
            return progress_in_percent;
        }

        public void setProgress_in_percent(Long progress_in_percent) {
            this.progress_in_percent = progress_in_percent;
        }

        public Long getStatus() {
            return status;
        }

        public void setStatus(Long status) {
            this.status = status;
        }
    }

}
