package com.jaka.framework.core.dingding.api.callback.user;

import com.jaka.framework.core.dingding.api.callback.CallBackUserBaseResult;

import java.util.List;

/**
 * @author ：james.liu
 * @date ：Created in 2021/12/30 11:10
 * @description：通讯录事件-通讯录企业部门删除
 * @version: 1.0
 */
public class OrgDeptRemoveCallBackResult extends CallBackUserBaseResult {

    /**
     * 部门发生变更的DeptId列表。
     */
    private List<String> DeptId;

    public List<String> getDeptId() {
        return DeptId;
    }

    public void setDeptId(List<String> deptId) {
        DeptId = deptId;
    }

}
