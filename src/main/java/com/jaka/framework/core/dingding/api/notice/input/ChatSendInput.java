package com.jaka.framework.core.dingding.api.notice.input;

import com.alibaba.fastjson.JSONObject;
import com.jaka.framework.core.dingding.base.AbstractAPIInput;
import okhttp3.FormBody;
import okhttp3.RequestBody;

/**
 * @author ：james.liu
 * @date ：Created in 2021/12/29 15:28
 * @description：发送消息到企业群
 * @version: 1.0
 */
public class ChatSendInput extends AbstractAPIInput {

    private String chatid;

    private JSONObject msg;

    public String getChatid() {
        return chatid;
    }

    public void setChatid(String chatid) {
        this.chatid = chatid;
    }

    public JSONObject getMsg() {
        return msg;
    }

    public void setMsg(JSONObject msg) {
        this.msg = msg;
    }

    /**
     * 描述：转换RequestBody对象
     */
    public final RequestBody toRequestBody() {
        final FormBody.Builder builder = new FormBody.Builder();
        builder.add("chatid", this.getChatid());
        builder.add("msg", this.getMsg().toJSONString());
        RequestBody r = builder.build();
        return r;
    }
}
