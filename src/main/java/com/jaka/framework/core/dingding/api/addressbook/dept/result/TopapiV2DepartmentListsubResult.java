package com.jaka.framework.core.dingding.api.addressbook.dept.result;

import com.jaka.framework.core.dingding.base.AbstractAPIResult;

import java.util.List;

/**
 * @author ：james.liu
 * @date ：Created in 2021/12/27 15:27
 * @description：获取部门列表
 * @version: 1.0
 */
public class TopapiV2DepartmentListsubResult extends AbstractAPIResult {

    /**
     * 部门列表。
     */
    private List<TopapiV2DepartmentListsub> result;

    public List<TopapiV2DepartmentListsub> getResult() {
        return result;
    }

    public void setResult(List<TopapiV2DepartmentListsub> result) {
        this.result = result;
    }

    public static class TopapiV2DepartmentListsub {
        /**
         * 部门ID。
         */
        private Long dept_id;
        /**
         * 部门名称。
         */
        private String name;
        /**
         * 父部门ID。
         */
        private Long parent_id;
        /**
         * 是否同步创建一个关联此部门的企业群：
         * true：创建
         * false：不创建
         */
        private boolean create_dept_group;
        /**
         * 部门群已经创建后，有新人加入部门是否会自动加入该群：
         * true：会自动入群
         * false：不会
         */
        private boolean auto_add_user;

        private String ext;

        public Long getDept_id() {
            return dept_id;
        }

        public void setDept_id(Long dept_id) {
            this.dept_id = dept_id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Long getParent_id() {
            return parent_id;
        }

        public void setParent_id(Long parent_id) {
            this.parent_id = parent_id;
        }

        public boolean isCreate_dept_group() {
            return create_dept_group;
        }

        public void setCreate_dept_group(boolean create_dept_group) {
            this.create_dept_group = create_dept_group;
        }

        public boolean isAuto_add_user() {
            return auto_add_user;
        }

        public void setAuto_add_user(boolean auto_add_user) {
            this.auto_add_user = auto_add_user;
        }

        public String getExt() {
            return ext;
        }

        public void setExt(String ext) {
            this.ext = ext;
        }
    }
}
