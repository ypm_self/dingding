package com.jaka.framework.core.dingding.api.addressbook.user.result;

import com.jaka.framework.core.dingding.base.AbstractAPIResult;

/**
 * @author ：james.liu
 * @date ：Created in 2021/12/27 18:33
 * @description：获取员工人数
 * @version: 1.0
 */
public class TopApiUserCountResult extends AbstractAPIResult {
    /**
     * 返回结果。
     */
    private CountUserResponse result;

    public CountUserResponse getResult() {
        return result;
    }

    public void setResult(CountUserResponse result) {
        this.result = result;
    }

    public static class CountUserResponse {
        /**
         * 员工数量。
         */
        private Long count;

        public Long getCount() {
            return count;
        }

        public void setCount(Long count) {
            this.count = count;
        }
    }
}
