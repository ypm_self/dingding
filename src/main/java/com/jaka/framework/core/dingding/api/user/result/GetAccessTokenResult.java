package com.jaka.framework.core.dingding.api.user.result;


import com.jaka.framework.core.dingding.base.AbstractAPIResult;

/**
 * @author ：james.liu
 * @date ：Created in 2021/12/14 18:52
 * @description：获取access_token 出参
 * @version: 1.0
 */
public class GetAccessTokenResult extends AbstractAPIResult {

    private String accessToken;

    private Integer expiresIn;

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public Integer getExpiresIn() {
        return expiresIn;
    }

    public void setExpiresIn(Integer expiresIn) {
        this.expiresIn = expiresIn;
    }
}
