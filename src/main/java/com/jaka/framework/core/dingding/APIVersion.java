package com.jaka.framework.core.dingding;

/**
 * @author ：james.liu
 * @date ：Created in 2021/12/14 15:36
 * @description： API版本管理
 * @version: 1.0
 */
final class APIVersion {

    public static final String version = "V1.0.0";
    public static final String author = "james.liu";
    public static final String createDate = "2021-12-14";
    public static final String updateDate = "2021-12-14";

    /**
     *
     */
    public APIVersion() {

    }

}
