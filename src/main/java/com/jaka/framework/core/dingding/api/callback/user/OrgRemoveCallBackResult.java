package com.jaka.framework.core.dingding.api.callback.user;

import com.jaka.framework.core.dingding.api.callback.CallBackUserBaseResult;

/**
 * @author ：james.liu
 * @date ：Created in 2021/12/30 11:24
 * @description：企业被解散
 * @version: 1.0
 */
public class OrgRemoveCallBackResult extends CallBackUserBaseResult {
}
