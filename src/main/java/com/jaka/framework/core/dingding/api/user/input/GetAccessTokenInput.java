package com.jaka.framework.core.dingding.api.user.input;


import com.jaka.framework.core.dingding.base.AbstractAPIInput;



/**
 * @author ：james.liu
 * @date ：Created in 2021/12/14 18:30
 * @description： 获取access_token  入参
 * @version: 1.0
 */
public class GetAccessTokenInput extends AbstractAPIInput {


}
