package com.jaka.framework.core.dingding.em;

/**
 * @author ：james.liu
 * @date ：Created in 2021/12/24 11:03
 * @description：
 * @version: 1.0
 */
public enum ResignStatusEnums {

    resign_status_1(1, "待离职"),
    resign_status_2(2, "已离职"),
    resign_status_3(3, "未离职"),
    resign_status_4(4, "发起离职审批但还未通过"),
    resign_status_5(5, "失效（离职流程被其他流程强制终止后的状态）"),;

    ResignStatusEnums(Integer type, String desc) {
        this.type = type;
        this.desc = desc;
    }

    /**
     * 类型编号
     */
    private Integer type;
    /**
     * 解释
     */
    private String desc;

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

}
