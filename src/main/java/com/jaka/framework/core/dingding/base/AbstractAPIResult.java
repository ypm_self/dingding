package com.jaka.framework.core.dingding.base;


import com.jaka.framework.common.core.model.AbstractToJsonString;

/**
 * @author ：james.liu
 * @date ：Created in 2021/12/14 16:37
 * @description：
 * @version: 1.0
 */
public class AbstractAPIResult implements AbstractToJsonString {
    /**
     * 描述：请求ID。
     **/
    private String requestId;
    /**
     * 描述：返回码，0代表成功
     **/
    private Integer errcode;
    /**
     * 描述：返回码描述。
     **/
    private String errmsg;

    private boolean success;

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public Integer getErrcode() {
        return errcode;
    }

    public void setErrcode(Integer errcode) {
        this.errcode = errcode;
    }

    public String getErrmsg() {
        return errmsg;
    }

    public void setErrmsg(String errmsg) {
        this.errmsg = errmsg;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }
}
