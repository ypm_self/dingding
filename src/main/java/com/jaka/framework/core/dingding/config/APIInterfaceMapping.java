package com.jaka.framework.core.dingding.config;


import com.jaka.framework.common.core.exception.EmptyValueException;
import com.jaka.framework.core.dingding.base.en.RequestMappering;
import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author ：james.liu
 * @date ：Created in 2021/12/14 16:00
 * @description：API接口参数映射
 * @version: 1.0
 */
@SuppressWarnings("all")
public final class APIInterfaceMapping {
    /**
     * 描述：输入接口 参数映射
     */
    private static final Map<String, Object> inputMapping = new ConcurrentHashMap<>();
    
    static {
        // 通讯录管理 start
        // 根据userId获取用户详情 -- 基本信息
        registerAPIMapping(RequestMappering.topapi_v2_user_get.getMapperName()
                , "CmdId"
                , RequestMappering.topapi_v2_user_get.getAppenMapper("?access_token={0}"));
        
        registerAPIMapping(RequestMappering.topapi_user_count.getMapperName()
                , "CmdId"
                , RequestMappering.topapi_user_count.getAppenMapper("?access_token={0}"));
        
        // 获取用户基本信息  -- 基本信息
        registerAPIMapping(RequestMappering.getuserinfo.getMapperName()
                , "CmdId"
                , RequestMappering.getuserinfo.getMapperName() + "?code={0}");
        // ***
        registerAPIMapping(RequestMappering.getuserinfo_bycode.getMapperName()
                , "CmdId"
                , RequestMappering.getuserinfo_bycode.getAppenMapper("?accessKey={0}&timestamp={1}&signature={2}"));
        //
        registerAPIMapping(RequestMappering.getbyunionid.getMapperName()
                , "CmdId"
                , RequestMappering.getbyunionid.getAppenMapper("?access_token={0}"));
        //获取企业内部应用的access_token
        registerAPIMapping(RequestMappering.gettoken.getMapperName()
                , "CmdId"
                , RequestMappering.gettoken.getMapperName() + "?appkey={0}&appsecret={1}");
        // 发送工作通知
        registerAPIMapping(
                RequestMappering.top_api_message_corp_conver_sation_async_send_v2.getMapperName()
                , "CmdId,agentId"
                , RequestMappering.top_api_message_corp_conver_sation_async_send_v2
                        .getAppenMapper("?access_token={0}"));
        
        
        // 文件存储 start
        registerAPIMapping(
                RequestMappering.media_upload.getMapperName()
                , "CmdId"
                , RequestMappering.media_upload
                        .getAppenMapper("?access_token={0}"));
        // 文件存储 end
        
        // 获取部门详情
        registerAPIMapping(
                RequestMappering.get_department.getMapperName()
                , "CmdId"
                , RequestMappering.get_department.getAppenMapper("?access_token={0}"));
        
        registerAPIMapping(
                RequestMappering.get_department_list.getMapperName()
                , "CmdId"
                , RequestMappering.get_department_list.getAppenMapper("?access_token={0}"));
        
        registerAPIMapping(
                RequestMappering.topapi_v2_department_list_parentbyuser.getMapperName()
                , "CmdId"
                , RequestMappering.topapi_v2_department_list_parentbyuser.getAppenMapper("?access_token={0}"));
        
        registerAPIMapping(
                RequestMappering.topapi_v2_department_listparentbydept.getMapperName()
                , "CmdId"
                , RequestMappering.topapi_v2_department_listparentbydept.getAppenMapper("?access_token={0}"));
        
        registerAPIMapping(
                RequestMappering.topapi_v2_department_listsub.getMapperName()
                , "CmdId"
                , RequestMappering.topapi_v2_department_listsub.getAppenMapper("?access_token={0}"));
        
        
        registerAPIMapping(
                RequestMappering.get_uset_listid.getMapperName()
                , "CmdId"
                , RequestMappering.get_uset_listid.getAppenMapper("?access_token={0}"));
        
        // 通讯录管理 -- 角色管理  start
        registerAPIMapping(
                RequestMappering.topapi_role_get_role_group.getMapperName()
                , "CmdId"
                , RequestMappering.topapi_role_get_role_group.getAppenMapper("?access_token={0}"));
        
        registerAPIMapping(
                RequestMappering.topapi_role_list.getMapperName()
                , "CmdId"
                , RequestMappering.topapi_role_list.getAppenMapper("?access_token={0}"));
        
        registerAPIMapping(
                RequestMappering.topapi_inactive_user_v2_get.getMapperName()
                , "CmdId"
                , RequestMappering.topapi_inactive_user_v2_get.getAppenMapper("?access_token={0}"));
        
        registerAPIMapping(
                RequestMappering.topapi_user_listadmin.getMapperName()
                , "CmdId"
                , RequestMappering.topapi_user_listadmin.getAppenMapper("?access_token={0}"));
        // 通讯录管理 -- 角色管理  end
        
        // 通讯录管理 end
        
        // 智能人事 start
        /**
         * 获取在职员工列表
         */
        registerAPIMapping(
                RequestMappering.get_employee_queryonjob.getMapperName()
                , "CmdId"
                , RequestMappering.get_employee_queryonjob.getAppenMapper("?access_token={0}"));
        /**
         * 获取待入职员工列表
         */
        registerAPIMapping(RequestMappering.hrm_employee_query_preentry.getMapperName()
                , "CmdId"
                , RequestMappering.hrm_employee_query_preentry.getAppenMapper("?access_token={0}"));
        /**
         * 添加企业待入职员工
         */
        registerAPIMapping(RequestMappering.hrm_employee_add_preentry.getMapperName()
                , "CmdId"
                , RequestMappering.hrm_employee_add_preentry.getAppenMapper("?access_token={0}"));
        /**
         * 获取离职员工列表
         */
        registerAPIMapping(RequestMappering.hrm_employee_query_dimission.getMapperName()
                , "CmdId"
                , RequestMappering.hrm_employee_query_dimission.getAppenMapper("?access_token={0}"));
        /**
         * 获取员工离职信息
         */
        registerAPIMapping(RequestMappering.hrm_employee_list_dimission.getMapperName()
                , "CmdId"
                , RequestMappering.hrm_employee_list_dimission.getAppenMapper("?access_token={0}"));
        
        
        // 智能人事 end
        
        
        registerAPIMapping(
                RequestMappering.auth_scopes.getMapperName()
                , "CmdId"
                , RequestMappering.auth_scopes.getAppenMapper("?access_token={0}"));
        
        // 通过免登码获取用户信息
        registerAPIMapping(
                RequestMappering.top_api_v2_user_get_user_info.getMapperName()
                , "CmdId"
                , RequestMappering.top_api_v2_user_get_user_info.getAppenMapper("?access_token={0}"));
        
    }
    
    /**
     * 设置接口拼接地址
     **/
    public static final String PERFIX_SPLICING = "-splicing";
    /**
     * 设置校验参数
     **/
    private static final String PREFIX_REQUIRED = "-required";
    
    public static final String SET_ACCESS_TOKEN = "?access_token={0}";
    
    
    /**
     * 描述：（接口输入值）获取API 必选参数列表
     */
    public static List<String> getRequiredListInput(final String cmdId) {
        Object ob = inputMapping.get(cmdId + PREFIX_REQUIRED);
        if (Objects.isNull(ob)) {
            return null;
        }
        String o = inputMapping.get(cmdId + PREFIX_REQUIRED).toString();
        String[] split = StringUtils.split(o, ",");
        if (Objects.isNull(split)) {
            return null;
        }
        if (split.length <= 0) {
            return null;
        }
        List<String> r = Arrays.asList(split);
        return r;
    }
    
    /**
     * 描述：获取API 接口拼接前缀
     *
     * @param cmdId
     * @return
     */
    public static String getSplicingInput(final String cmdId) {
        Object result = inputMapping.get(cmdId + PERFIX_SPLICING);
        if (Objects.isNull(result)) {
            throw new EmptyValueException("获取接口前缀有误！");
        }
        return result.toString();
    }
    
    /**
     * 注册API映射表
     *
     * @param cmdId         接口名称/接口路径
     * @param inputRequired 入参需要校验部分
     */
    private static void registerAPIMapping(final String cmdId,
                                           final String inputRequired,
                                           final String inputSplicing) {
        inputMapping.put(cmdId + PERFIX_SPLICING, inputSplicing.toString());
        inputMapping.put(cmdId + PREFIX_REQUIRED, inputRequired.toString());
    }
    
    
}
