package com.jaka.framework.core.dingding.api.addressbook.dept.input;

import com.jaka.framework.core.dingding.base.AbstractAPIInput;
import okhttp3.FormBody;
import okhttp3.RequestBody;

/**
 * @author ：james.liu
 * @date ：Created in 2021/12/27 16:19
 * @description：获取指定用户的所有父部门列表
 * @version: 1.0
 */
public class TopApiV2DepartmentListParentByuUerInput extends AbstractAPIInput {

    /**
     * 要查询的用户的userid。
     */
    private Long userid;

    public Long getUserid() {
        return userid;
    }

    public void setUserid(Long userid) {
        this.userid = userid;
    }

    /**
     * 描述：转换RequestBody对象
     */
    public final RequestBody toRequestBody() {
        RequestBody r = null;
        final FormBody.Builder builder = new FormBody.Builder();
        builder.add("userid", this.getUserid().toString());
        r = builder.build();
        return r;
    }
}
