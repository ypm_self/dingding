package com.jaka.framework.core.dingding.api.addressbook;

import com.jaka.framework.common.core.base.IHttpUtils;
import com.jaka.framework.core.dingding.api.addressbook.dept.input.*;
import com.jaka.framework.core.dingding.api.addressbook.dept.result.*;
import com.jaka.framework.core.dingding.api.addressbook.role.input.TopApiRoleListInput;
import com.jaka.framework.core.dingding.api.addressbook.role.result.TopApiRoleListResult;
import com.jaka.framework.core.dingding.api.addressbook.user.input.TopApiUserCountInput;
import com.jaka.framework.core.dingding.api.addressbook.user.input.TopApiV2UserGetInput;
import com.jaka.framework.core.dingding.api.addressbook.user.input.TopApinactiveUserV2GetInput;
import com.jaka.framework.core.dingding.api.addressbook.user.result.TopApiUserCountResult;
import com.jaka.framework.core.dingding.api.addressbook.user.result.TopApiV2UserGetResult;
import com.jaka.framework.core.dingding.api.addressbook.user.result.TopApinactiveUserV2GetResult;
import com.jaka.framework.core.dingding.api.user.input.TopApiUserListid;
import com.jaka.framework.core.dingding.api.user.result.TopApiUserListIdResult;
import com.jaka.framework.core.dingding.base.DefaultAPIActionProxy;
import com.jaka.framework.core.dingding.base.en.RequestMappering;
import com.jaka.framework.core.dingding.config.APIClientConfiguration;

/**
 * @author ：james.liu
 * @date ：Created in 2021/12/27 10:56
 * @description：通讯录管理
 * @version: 1.0
 */
public class AddressBookClient {


    private final IHttpUtils httpUtils;
    private final APIClientConfiguration configuration;

    public AddressBookClient(final APIClientConfiguration configuration, final IHttpUtils httpUtils) {
        this.configuration = configuration;
        this.httpUtils = httpUtils;
    }

    /**
     * 获取角色列表
     *
     * @param input
     * @return
     * @throws Exception
     */
    public TopApiRoleListResult topApiRoleList(final TopApiRoleListInput input) throws Exception {
        input.setCmdId(RequestMappering.topapi_role_list.getMapperName());
        return DefaultAPIActionProxy.getInstance().doAction(input,
                configuration, httpUtils, TopApiRoleListResult.class, input.toRequestBody());
    }

    /**
     * 获取部门详情
     *
     * @param input
     * @return
     * @throws Exception
     */
    public TopapiV2DepartmentGetResult getDepartment(final TopapiV2DepartmentGetInput input) throws Exception {
        input.setCmdId(RequestMappering.get_department.getMapperName());
        return DefaultAPIActionProxy.getInstance().doAction(input, configuration, httpUtils, TopapiV2DepartmentGetResult.class, input.toRequestBody());
    }

    /**
     * 获取子部门ID列表
     *
     * @param input
     * @return
     * @throws Exception
     */
    public TopapiV2DepartmentListsubidResult getDepartmentList(final TopapiV2DepartmentListsubidInput input) throws Exception {
        input.setCmdId(RequestMappering.get_department_list.getMapperName());
        return DefaultAPIActionProxy.getInstance().doAction(input, configuration, httpUtils, TopapiV2DepartmentListsubidResult.class, input.toRequestBody());
    }


    /**
     * 获取部门用户userid列表
     *
     * @param input
     * @return
     * @throws Exception
     */
    public TopApiUserListIdResult getUserListId(final TopApiUserListid input) throws Exception {
        input.setCmdId(RequestMappering.get_uset_listid.getMapperName());
        return DefaultAPIActionProxy.getInstance().doAction(input, configuration, httpUtils, TopApiUserListIdResult.class, input.toRequestBody());
    }

    /**
     * 获取部门列表
     *
     * @param input
     * @return
     * @throws Exception
     */
    public TopapiV2DepartmentListsubResult topapiV2DepartmentListsub(final TopapiV2DepartmentListsubInput input) throws Exception {
        input.setCmdId(RequestMappering.topapi_v2_department_listsub.getMapperName());
        return DefaultAPIActionProxy.getInstance().doAction(input, configuration, httpUtils, TopapiV2DepartmentListsubResult.class, input.toRequestBody());
    }

    /**
     * 获取指定部门的所有父部门列表
     *
     * @param input
     * @return
     * @throws Exception
     */
    public TopapiV2DepartmentListParentByDeptResult topapiV2DepartmentListParentByDept(final TopapiV2DepartmentListParentByDeptInput input) throws Exception {
        input.setCmdId(RequestMappering.topapi_v2_department_listparentbydept.getMapperName());
        return DefaultAPIActionProxy.getInstance().doAction(input, configuration, httpUtils, TopapiV2DepartmentListParentByDeptResult.class, input.toRequestBody());
    }

    /**
     * 获取指定用户的所有父部门列表
     *
     * @param input
     * @return
     * @throws Exception
     */
    public TopApiV2DepartmentListParentByuUerResult topApiV2DepartmentListParentByuUer(final TopApiV2DepartmentListParentByuUerInput input) throws Exception {
        input.setCmdId(RequestMappering.topapi_v2_department_list_parentbyuser.getMapperName());
        return DefaultAPIActionProxy.getInstance().doAction(input, configuration, httpUtils, TopApiV2DepartmentListParentByuUerResult.class, input.toRequestBody());
    }

    /**
     * 根据userId获取用户详情 -- 基本信息
     *
     * @param input
     * @return
     * @throws Exception
     */
    public TopApiV2UserGetResult topApiV2UserGet(final TopApiV2UserGetInput input) throws Exception {
        input.setCmdId(RequestMappering.topapi_v2_user_get.getMapperName());
        return DefaultAPIActionProxy.getInstance().doAction(input, configuration, httpUtils, TopApiV2UserGetResult.class, input.toRequestBody());
    }

    /**
     * 获取员工人数
     *
     * @param input
     * @return
     * @throws Exception
     */
    public TopApiUserCountResult topApiUserCount(final TopApiUserCountInput input) throws Exception {
        input.setCmdId(RequestMappering.topapi_user_count.getMapperName());
        return DefaultAPIActionProxy.getInstance().doAction(input, configuration, httpUtils, TopApiUserCountResult.class, input.toRequestBody());
    }

    /**
     * 获取未登录钉钉的员工列表
     *
     * @param input
     * @return
     * @throws Exception
     */
    public TopApinactiveUserV2GetResult topApinactiveUserV2Get(final TopApinactiveUserV2GetInput input) throws Exception {
        input.setCmdId(RequestMappering.topapi_inactive_user_v2_get.getMapperName());
        return DefaultAPIActionProxy.getInstance().doAction(input, configuration, httpUtils, TopApinactiveUserV2GetResult.class, input.toRequestBody());
    }

}

