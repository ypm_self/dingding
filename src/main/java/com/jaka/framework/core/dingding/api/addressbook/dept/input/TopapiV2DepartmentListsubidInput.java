package com.jaka.framework.core.dingding.api.addressbook.dept.input;

import com.jaka.framework.core.dingding.base.AbstractAPIInput;
import okhttp3.FormBody;
import okhttp3.RequestBody;

import java.util.Objects;

/**
 * @author ：james.liu
 * @date ：Created in 2021/12/20 9:01
 * @description：获取子部门ID列表
 * @version: 1.0
 */
public class TopapiV2DepartmentListsubidInput extends AbstractAPIInput {

    /**
     * 父部门ID，根部门传1
     */
    private Long deptId;

    public Long getDeptId() {
        return deptId;
    }

    public void setDeptId(Long deptId) {
        this.deptId = deptId;
    }

    /**
     * 描述：转换RequestBody对象
     */
    public final RequestBody toRequestBody() {
        RequestBody r = null;
        final FormBody.Builder builder = new FormBody.Builder();
        if(Objects.nonNull(this.getDeptId())){
            builder.add("dept_id", this.getDeptId().toString());
        }
        r = builder.build();
        return r;
    }
}
