package com.jaka.framework.core.dingding.api.callback.bpms;

/**
 * @author ：james.liu
 * @date ：Created in 2021/12/30 11:51
 * @description：审批任务开始,审批任务结束
 * @version: 1.0
 */
public class BpmsTaskChangeCallBackResult extends BpmsTaskChangeBaseResult {

    /**
     * 审批结束时间。
     */
    private Long finishTime;
    /**
     * agree：同意
     * refuse：拒绝
     */
    private String result;
    /**
     * remark表示操作时写的评论内容。
     */
    private String remark;

    public Long getFinishTime() {
        return finishTime;
    }

    public void setFinishTime(Long finishTime) {
        this.finishTime = finishTime;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
