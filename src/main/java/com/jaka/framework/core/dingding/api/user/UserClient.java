package com.jaka.framework.core.dingding.api.user;


import com.jaka.framework.common.core.base.IHttpUtils;
import com.jaka.framework.core.dingding.api.user.input.GetAccessTokenInput;
import com.jaka.framework.core.dingding.api.user.input.GetUserInfoInput;
import com.jaka.framework.core.dingding.api.user.result.GetAccessTokenResult;
import com.jaka.framework.core.dingding.api.user.result.GetUserInfoResult;
import com.jaka.framework.core.dingding.base.DefaultAPIActionProxy;
import com.jaka.framework.core.dingding.base.en.RequestMappering;
import com.jaka.framework.core.dingding.config.APIClientConfiguration;

import java.io.IOException;

/**
 * @author ：james.liu
 * @date ：Created in 2021/12/14 17:50
 * @description：
 * @version: 1.0
 */
public class UserClient {

    private final IHttpUtils httpUtils;
    private final APIClientConfiguration configuration;

    public UserClient(final APIClientConfiguration configuration, final IHttpUtils httpUtils) {
        this.configuration = configuration;
        this.httpUtils = httpUtils;
    }

    /**
     * 获取用的access_token
     *
     * @param input
     * @return
     * @throws IOException
     */
    public GetAccessTokenResult getAccessToken(final GetAccessTokenInput input) throws Exception {
        input.setCmdId(RequestMappering.gettoken.getMapperName());
        return DefaultAPIActionProxy.getInstance().doAction(input, configuration, httpUtils, GetAccessTokenResult.class);
    }


    /**
     * 第三方企业应用免登
     *
     * @param input
     * @return
     * @throws Exception
     */
    public GetUserInfoResult getUserInfo(final GetUserInfoInput input) throws Exception {
        input.setCmdId(RequestMappering.getuserinfo.getMapperName());
        return DefaultAPIActionProxy.getInstance().doGetAction(input, configuration, httpUtils,
                GetUserInfoResult.class,
                input.getAccessToken());
    }

}
