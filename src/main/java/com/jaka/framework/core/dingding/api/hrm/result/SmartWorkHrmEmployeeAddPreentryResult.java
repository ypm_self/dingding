package com.jaka.framework.core.dingding.api.hrm.result;

import com.jaka.framework.core.dingding.base.AbstractAPIResult;

/**
 * @author ：james.liu
 * @date ：Created in 2021/12/24 10:38
 * @description：
 * @version: 1.0
 */
public class SmartWorkHrmEmployeeAddPreentryResult extends AbstractAPIResult {
    /**
     * 员工ID。
     */
    private String userid;

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }
}
