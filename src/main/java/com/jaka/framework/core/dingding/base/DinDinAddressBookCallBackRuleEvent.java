package com.jaka.framework.core.dingding.base;


import com.alibaba.fastjson.JSONObject;

/**
 * @author ：james.liu
 * @date ：Created in 2021/12/24 13:02
 * @description：钉钉通讯录回调时间规则抽象类
 * @version: 1.0
 */
public interface DinDinAddressBookCallBackRuleEvent {

    /**
     * 抽象通讯录回调抽象方法
     *
     * @param input
     */
    void abstractAddressBookCallBack(JSONObject input) throws Exception;

}
