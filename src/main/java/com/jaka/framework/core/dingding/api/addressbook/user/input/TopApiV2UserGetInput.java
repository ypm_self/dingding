package com.jaka.framework.core.dingding.api.addressbook.user.input;

import com.jaka.framework.core.dingding.base.AbstractAPIInput;
import okhttp3.FormBody;
import okhttp3.RequestBody;

/**
 * @author ：james.liu
 * @date ：Created in 2021/12/14 16:44
 * @description：根据userId获取用户详情,基本信息 输入参数
 * @version: 1.0
 */
public class TopApiV2UserGetInput extends AbstractAPIInput {

    private String userid;

    private String language = "zh_CN";

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    /**
     * 描述：转换RequestBody对象
     */
    public final RequestBody toRequestBody() {
        RequestBody r = null;
        final FormBody.Builder builder = new FormBody.Builder();
        builder.add("userid", this.getUserid());
        builder.add("language", this.getLanguage());
        r = builder.build();
        return r;
    }
}
