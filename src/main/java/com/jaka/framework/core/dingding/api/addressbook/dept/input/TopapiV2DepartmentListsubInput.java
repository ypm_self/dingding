package com.jaka.framework.core.dingding.api.addressbook.dept.input;

import com.jaka.framework.core.dingding.base.AbstractAPIInput;
import okhttp3.FormBody;
import okhttp3.RequestBody;

import java.util.Objects;

/**
 * @author ：james.liu
 * @date ：Created in 2021/12/27 15:26
 * @description：获取部门列表
 * @version: 1.0
 */
public class TopapiV2DepartmentListsubInput extends AbstractAPIInput {

    /**
     * 父部门ID
     * 企业内部应用，可调用获取部门列表获取
     * 第三方企业应用，可调用获取部门列表获取
     */
    private Long deptId;
    /**
     * 通讯录语言：
     * zh_CN（默认）：中文
     * en_US：英文
     */
    private String language = "zh_CN";

    public Long getDeptId() {
        return deptId;
    }

    public void setDeptId(Long deptId) {
        this.deptId = deptId;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    /**
     * 描述：转换RequestBody对象
     */
    public final RequestBody toRequestBody() {
        RequestBody r = null;
        final FormBody.Builder builder = new FormBody.Builder();
        if (Objects.nonNull(this.getDeptId())) {
            builder.add("dept_id", this.getDeptId().toString());
        }
        builder.add("language", this.getLanguage());
        r = builder.build();
        return r;
    }
}
