package com.jaka.framework.core.dingding.api.notice.input.message;

/**
 * @author ：james.liu
 * @date ：Created in 2021/12/29 10:07
 * @description：
 * @version: 1.0
 */
public enum MsgtypeEnums {
    text("text", "文本消息"),
    image("image", "图片消息"),
    voice("voicev", "语音消息"),
    file("file", "文件消息"),
    link("link", "链接消息"),
    oa("oa", "OA消息"),
    markdown("markdown", "markdown消息"),
    action_card("action_card", "卡片消息"),;

    private String type;

    private String desc;

    /**
     * 推送消息
     *
     * @param type 推送消息类型
     * @param desc 类型描述
     */
    MsgtypeEnums(String type, String desc) {
        this.type = type;
        this.desc = desc;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
