package com.jaka.framework.core.dingding.api.notice.input;

import com.jaka.framework.core.dingding.api.notice.input.message.NoticeBaseModel;
import com.jaka.framework.core.dingding.base.AbstractAPIInput;
import okhttp3.FormBody;
import okhttp3.RequestBody;
import org.apache.commons.lang3.StringUtils;

import java.util.Objects;

/**
 * @author ：james.liu
 * @date ：Created in 2021/12/17 11:37
 * @description：发送工作通知入参
 * @version: 1.0
 */
public class TopApiMessageCorpConverSationAsyncSendV2Input<T extends NoticeBaseModel> extends AbstractAPIInput {

    /**
     * 发送消息时使用的微应用的AgentID。
     * 企业内部应用可在开发者后台的应用详情页面查看。
     * 第三方企业应用可调用获取企业授权信息接口获取。
     */
    private Long agentId;
    /**
     * 接收者的userid列表，最大用户列表长度100。
     */
    private String useridList;
    /**
     * 接收者的部门id列表，最大列表长度20。
     * 接收者是部门ID时，包括子部门下的所有用户。
     */
    private String deptIdList;
    /**
     * 消息内容，最长不超过2048个字节，支持以下消息类型：
     * 文本消息
     * 图片消息
     * 语音消息
     * 文件消息
     * 链接消息
     * OA消息
     * 注意 OA消息支持通过status_bar参数设置消息的状态文案和颜色，消息发送后可调用更新工作通知状态栏接口更新消息状态和颜色。
     * Markdown消息
     * 卡片消息
     * 返回参数
     */
    private T msg;
    /**
     * 否发送给企业全部用户。
     * d
     * 说明 当设置为false时必须指定userid_list或dept_id_list其中一个参数的值。
     */
    private Boolean toAllUser = false;

    /**
     * 描述：转换RequestBody对象
     */
    public final RequestBody toRequestBody() {
        final FormBody.Builder builder = new FormBody.Builder();
        builder.add("agent_id", this.getAgentId().toString());
        if (StringUtils.isNotEmpty(this.getUseridList())) {
            builder.add("userid_list", this.getUseridList());
        }
        String deptIdList = this.getDeptIdList();
        if (StringUtils.isNotEmpty(deptIdList)) {
            builder.add("dept_id_list", deptIdList);
        }
        builder.add("to_all_user", this.getToAllUser().toString());
        if (Objects.nonNull(this.getMsg())) {
            builder.add("msg", this.getMsg().toJsonString());
        }
        RequestBody r = builder.build();
        return r;
    }


    public Long getAgentId() {
        return agentId;
    }

    public void setAgentId(Long agentId) {
        this.agentId = agentId;
    }

    public String getUseridList() {
        return useridList;
    }

    public void setUseridList(String useridList) {
        this.useridList = useridList;
    }

    public String getDeptIdList() {
        return deptIdList;
    }

    public void setDeptIdList(String deptIdList) {
        this.deptIdList = deptIdList;
    }

    public T getMsg() {
        return msg;
    }

    public void setMsg(T msg) {
        this.msg = msg;
    }

    public Boolean getToAllUser() {
        return toAllUser;
    }

    public void setToAllUser(Boolean toAllUser) {
        this.toAllUser = toAllUser;
    }
}
