package com.jaka.framework.core.dingding.api.notice.input.message;

import java.util.List;

/**
 * @author ：james.liu
 * @date ：Created in 2021/12/29 11:12
 * @description：OA消息
 * @version: 1.0
 */
public class NoticeOA extends NoticeBaseModel {

    /**
     *
     */
    private OA oa;

    public OA getOa() {
        return oa;
    }

    public void setOa(OA oa) {
        this.oa = oa;
    }

    public static class OA{
        /**
         *息点击链接地址，当发送消息为小程序时支持小程序跳转链接。
         *
         * 企业内部应用参考消息链接说明
         *
         * 第三方企业应用参考消息链接说明
         */
        private String message_url;

        private Body body;

        private Head head;

        public String getMessage_url() {
            return message_url;
        }

        public void setMessage_url(String message_url) {
            this.message_url = message_url;
        }

        public Body getBody() {
            return body;
        }

        public void setBody(Body body) {
            this.body = body;
        }

        public Head getHead() {
            return head;
        }

        public void setHead(Head head) {
            this.head = head;
        }
    }


    public static class Body{

        private String title;

        private String content;
        /**
         * 消息体中的图片，支持图片资源@mediaId。建议宽600像素 x 400像素，宽高比3 : 2。
         * 企业内部应用通过上传媒体文件接口获取
         * 第三方企业应用通过上传媒体文件接口获取
         */
        private String image;
        /**
         * 自定义的附件数目。此数字仅供显示，钉钉不作验证。
         */
        private String file_count;
        /**
         * 自定义的作者名字。
         */
        private String author;
        /**
         * 消息体的表单，最多显示6个，超过会被隐藏。
         */
        private List<Form> form;
        /**
         * 单行富文本信息。
         */
        private Rich rich;

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getFile_count() {
            return file_count;
        }

        public void setFile_count(String file_count) {
            this.file_count = file_count;
        }

        public String getAuthor() {
            return author;
        }

        public void setAuthor(String author) {
            this.author = author;
        }

        public List<Form> getForm() {
            return form;
        }

        public void setForm(List<Form> form) {
            this.form = form;
        }

        public Rich getRich() {
            return rich;
        }

        public void setRich(Rich rich) {
            this.rich = rich;
        }
    }

    public static class Form{
        /**
         * 消息体的关键字。
         */
        private String key;
        /**
         * 消息体的关键字对应的值。
         */
        private String value;

        public String getKey() {
            return key;
        }

        public void setKey(String key) {
            this.key = key;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }
    }


    public static class Rich{
        /**
         *单行富文本信息的数目。
         */
        private String num;
        /**
         *单行富文本信息的单位。
         */
        private String unit;

        public String getNum() {
            return num;
        }

        public void setNum(String num) {
            this.num = num;
        }

        public String getUnit() {
            return unit;
        }

        public void setUnit(String unit) {
            this.unit = unit;
        }
    }


    /**
     * 消息头部内容。
     */
    public static class Head {
        /**
         * 消息头部的背景颜色。
         * 长度限制为8个英文字符，其中前2为表示透明度，后6位表示颜色值。不要添加0x。
         */
        private String bgcolor = "FFBBBBBB";
        /**
         * 消息的头部标题 (向普通会话发送时有效，向企业会话发送时会被替换为微应用的名字)。
         * 长度限制为最多10个字符。
         */
        private String text;

        public String getBgcolor() {
            return bgcolor;
        }

        public void setBgcolor(String bgcolor) {
            this.bgcolor = bgcolor;
        }

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }
    }
}
