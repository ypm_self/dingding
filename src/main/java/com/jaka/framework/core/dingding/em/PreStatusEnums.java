package com.jaka.framework.core.dingding.em;

/**
 * @author ：james.liu
 * @date ：Created in 2021/12/24 11:01
 * @description：
 * @version: 1.0
 */
public enum PreStatusEnums {

    pre_status_1(1, "待入职"),
    pre_status_2(2, "试用期"),
    pre_status_3(3, "正式"),;

    PreStatusEnums(Integer type, String desc) {
        this.type = type;
        this.desc = desc;
    }

    /**
     * 类型编号
     */
    private Integer type;
    /**
     * 解释
     */
    private String desc;

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
