package com.jaka.framework.core.dingding.api.addressbook.user.result;

import com.jaka.framework.core.dingding.base.AbstractAPIResult;

import java.util.List;

/**
 * @author ：james.liu
 * @date ：Created in 2022/1/6 13:55
 * @description：获取未登录钉钉的员工列表
 * @version: 1.0
 */
public class TopApinactiveUserV2GetResult  extends AbstractAPIResult {

    private PageVo result;

    public PageVo getResult() {
        return result;
    }

    public void setResult(PageVo result) {
        this.result = result;
    }

    public static class PageVo{

        private Long nextCursor;

        private List<String> list;

        private boolean has_more;

        public Long getNextCursor() {
            return nextCursor;
        }

        public void setNextCursor(Long nextCursor) {
            this.nextCursor = nextCursor;
        }

        public List<String> getList() {
            return list;
        }

        public void setList(List<String> list) {
            this.list = list;
        }

        public boolean isHas_more() {
            return has_more;
        }

        public void setHas_more(boolean has_more) {
            this.has_more = has_more;
        }
    }
}
