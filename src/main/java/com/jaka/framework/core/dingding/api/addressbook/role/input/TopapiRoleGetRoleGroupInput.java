package com.jaka.framework.core.dingding.api.addressbook.role.input;

import com.jaka.framework.core.dingding.base.AbstractAPIInput;

/**
 * @author ：james.liu
 * @date ：Created in 2021/12/27 10:47
 * @description：获取角色组列表入参
 * @version: 1.0
 */
public class TopapiRoleGetRoleGroupInput extends AbstractAPIInput {


}
