package com.jaka.framework.core.dingding.api.chat.result;

import com.jaka.framework.core.dingding.base.AbstractAPIResult;

/**
 * @author ：james.liu
 * @date ：Created in 2021/12/30 17:33
 * @description：查询群成员
 * @version: 1.0
 */
public class TopApiImChatSceneGroupMemberGetResult extends AbstractAPIResult {
}
