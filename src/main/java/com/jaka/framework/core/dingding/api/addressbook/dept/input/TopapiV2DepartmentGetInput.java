package com.jaka.framework.core.dingding.api.addressbook.dept.input;

import com.jaka.framework.core.dingding.base.AbstractAPIInput;
import okhttp3.FormBody;
import okhttp3.RequestBody;

/**
 * @author ：james.liu
 * @date ：Created in 2021/12/17 13:44
 * @description：获取部门详情入参
 * @version: 1.0
 */
public class TopapiV2DepartmentGetInput extends AbstractAPIInput {

    /**
     * 部门ID，根部门ID为1
     * 企业内部应用，可调用获取部门列表获取
     * 钉钉三方企业应用，可调用获取部门列表获取
     */
    private Long deptId;

    /**
     * 通讯录语言：
     * zh_CN（默认）：中文
     * en_US：英文
     */
    private String language = "zh_CN";

    /**
     * 描述：转换RequestBody对象
     */
    public final RequestBody toRequestBody() {
        RequestBody r;
        final FormBody.Builder builder = new FormBody.Builder();
        builder.add("dept_id", this.getDeptId().toString());
        builder.add("language", this.getLanguage());
        r = builder.build();
        return r;
    }


    public Long getDeptId() {
        return deptId;
    }

    public void setDeptId(Long deptId) {
        this.deptId = deptId;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }
}
