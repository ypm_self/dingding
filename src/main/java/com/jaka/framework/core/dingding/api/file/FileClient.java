package com.jaka.framework.core.dingding.api.file;

import com.jaka.framework.common.core.base.IHttpUtils;
import com.jaka.framework.core.dingding.api.file.input.MediaUploadInput;
import com.jaka.framework.core.dingding.api.file.result.MediaUploadResult;
import com.jaka.framework.core.dingding.base.DefaultAPIActionProxy;
import com.jaka.framework.core.dingding.config.APIClientConfiguration;

/**
 * @author ：james.liu
 * @date ：Created in 2021/12/29 10:47
 * @description：文件存储客户端
 * @version: 1.0
 */
public class FileClient {

    private final IHttpUtils httpUtils;
    private final APIClientConfiguration configuration;

    public FileClient(final APIClientConfiguration configuration, final IHttpUtils httpUtils) {
        this.configuration = configuration;
        this.httpUtils = httpUtils;
    }

    /**
     * 上传媒体文件
     *
     * @param input
     * @return
     * @throws Exception
     */
//    public MediaUploadResult mediaUpload(final MediaUploadInput input) throws Exception {
//        return DefaultAPIActionProxy.getInstance().doAction(input,
//                configuration,
//                httpUtils,
//                MediaUploadResult.class,
//                input.toRequestBody());
//    }

}
