package com.jaka.framework.core.dingding.api.callback.attendance;


import com.jaka.framework.core.dingding.api.callback.CallBackBaseResult;

/**
 * @author ：james.liu
 * @date ：Created in 2021/12/30 9:37
 * @description：考勤事件-员工加班事件
 * @version: 1.0
 */
public class AttendanCeverTimeDurationCallBackResult extends CallBackBaseResult {
    /**
     * 数据列表
     */
    private AttendanCeverTimeDurationDataList DataList;

    public AttendanCeverTimeDurationDataList getDataList() {
        return DataList;
    }

    public void setDataList(AttendanCeverTimeDurationDataList dataList) {
        DataList = dataList;
    }

    public static class AttendanCeverTimeDurationDataList {
        /**
         * 企业的corpId。
         */
        private String corpId;
        /**
         * 员工的userid。
         */
        private String userid;
        /**
         * 加班时长，单位天
         */
        private String overtimeDay;
        /**
         * 加班时长，单位小时
         */
        private Double overtimeHour;
        /**
         * 加班日期
         */
        private Float workDate;

        public String getCorpId() {
            return corpId;
        }

        public void setCorpId(String corpId) {
            this.corpId = corpId;
        }

        public String getUserid() {
            return userid;
        }

        public void setUserid(String userid) {
            this.userid = userid;
        }

        public String getOvertimeDay() {
            return overtimeDay;
        }

        public void setOvertimeDay(String overtimeDay) {
            this.overtimeDay = overtimeDay;
        }

        public Double getOvertimeHour() {
            return overtimeHour;
        }

        public void setOvertimeHour(Double overtimeHour) {
            this.overtimeHour = overtimeHour;
        }

        public Float getWorkDate() {
            return workDate;
        }

        public void setWorkDate(Float workDate) {
            this.workDate = workDate;
        }
    }

}
