package com.jaka.framework.core.dingding.api.callback.user;

import com.jaka.framework.core.dingding.api.callback.CallBackUserBaseResult;

import java.util.List;

/**
 * @author ：james.liu
 * @date ：Created in 2021/12/30 14:26
 * @description：通讯录企业部门修改
 * @version: 1.0
 */
public class OrgDeptModifyCallBackResult extends CallBackUserBaseResult {

    /**
     * 部门发生变更的DeptId列表。
     */
    private List<String> DeptId;

    public List<String> getDeptId() {
        return DeptId;
    }

    public void setDeptId(List<String> deptId) {
        DeptId = deptId;
    }
}
