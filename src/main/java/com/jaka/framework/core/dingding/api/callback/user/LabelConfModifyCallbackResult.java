package com.jaka.framework.core.dingding.api.callback.user;

import com.jaka.framework.core.dingding.api.callback.CallBackUserBaseResult;

import java.util.List;

/**
 * @author ：james.liu
 * @date ：Created in 2021/12/30 11:21
 * @description：修改角色或者角色组
 * @version: 1.0
 */
public class LabelConfModifyCallbackResult extends CallBackUserBaseResult {
    /**
     * 变更后的角色或者角色组。
     */
    private List<String> PostLabelList;
    /**
     * 变更前的角色或者角色组。
     */
    private List<String> PreLabelList;
    /**
     * 角色或者角色组id列表。
     */
    private List<Long> LabelIdList;
    /**
     *
     */
    private String scope;

    public List<String> getPostLabelList() {
        return PostLabelList;
    }

    public void setPostLabelList(List<String> postLabelList) {
        PostLabelList = postLabelList;
    }

    public List<String> getPreLabelList() {
        return PreLabelList;
    }

    public void setPreLabelList(List<String> preLabelList) {
        PreLabelList = preLabelList;
    }

    public List<Long> getLabelIdList() {
        return LabelIdList;
    }

    public void setLabelIdList(List<Long> labelIdList) {
        LabelIdList = labelIdList;
    }

    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }
}
