package com.jaka.framework.core.dingding.api.hrm.input;

import com.jaka.framework.core.dingding.base.AbstractAPIInput;
import okhttp3.FormBody;
import okhttp3.RequestBody;

/**
 * @author ：james.liu
 * @date ：Created in 2021/12/24 10:47
 * @description：获取员工离职信息入参
 * @version: 1.0
 */
public class SmartWorkHrmEmployeeListDimissionInput extends AbstractAPIInput {

    /**
     * 要查询的离职员工userid，多个员工用逗号分隔，最大长度50。
     * <p>
     * 企业内部应用，通过获取离职员工列表接口获取。
     * <p>
     * 第三方企业应用，通过获取离职员工列表接口获取。
     */
    private String userid_list;

    public String getUserid_list() {
        return userid_list;
    }

    public void setUserid_list(String userid_list) {
        this.userid_list = userid_list;
    }

    /**
     * 描述：转换RequestBody对象
     */
    public final RequestBody toRequestBody() {
        final FormBody.Builder builder = new FormBody.Builder();
        builder.add("userid_list", this.getUserid_list());
        RequestBody r = builder.build();
        return r;
    }
}
