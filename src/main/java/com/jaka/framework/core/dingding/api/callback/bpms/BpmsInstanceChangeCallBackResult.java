package com.jaka.framework.core.dingding.api.callback.bpms;

/**
 * @author ：james.liu
 * @date ：Created in 2021/12/30 11:30
 * @description：审批实例开始,审批实例结束|终止
 * @version: 1.0
 */
public class BpmsInstanceChangeCallBackResult extends BpmsTaskChangeBaseResult{
    /**
     * 审批实例url，可在钉钉内跳转到审批页面。
     */
    private String url;
    /**
     * 审批结束时间
     */
    private Long finishTime;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Long getFinishTime() {
        return finishTime;
    }

    public void setFinishTime(Long finishTime) {
        this.finishTime = finishTime;
    }
}
