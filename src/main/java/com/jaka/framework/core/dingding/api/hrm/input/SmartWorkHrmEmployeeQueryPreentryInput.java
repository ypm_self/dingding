package com.jaka.framework.core.dingding.api.hrm.input;

import com.jaka.framework.core.dingding.base.AbstractAPIInput;
import okhttp3.FormBody;
import okhttp3.RequestBody;

/**
 * @author ：james.liu
 * @date ：Created in 2021/12/24 9:59
 * @description：获取待入职员工列表入参
 * @version: 1.0
 */
public class SmartWorkHrmEmployeeQueryPreentryInput extends AbstractAPIInput {

    /**
     * 分页游标，从0开始。根据返回结果里的next_cursor是否为空来判断是否还有下一页，
     * 且再次调用时offset设置成next_cursor的值。分页游标，
     * 从0开始。根据返回结果里的next_cursor是否为空来判断是否还有下一页，
     * 且再次调用时offset设置成next_cursor的值。
     */
    private Integer offset = 0;
    /**
     * 分页大小，最大50。
     */
    private Integer size = 50;

    public Integer getOffset() {
        return offset;
    }

    public void setOffset(Integer offset) {
        this.offset = offset;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    /**
     * 描述：转换RequestBody对象
     */
    public final RequestBody toRequestBody() {
        final FormBody.Builder builder = new FormBody.Builder();
        builder.add("offset", this.getOffset().toString());
        builder.add("size", this.getSize().toString());
        RequestBody r = builder.build();
        return r;
    }
}
