package com.jaka.framework.core.dingding.base;

import com.jaka.framework.common.core.exception.EmptyValueException;
import com.jaka.framework.common.core.model.AbstractToJsonString;
import com.jaka.framework.common.core.model.BeanUtils;
import com.jaka.framework.common.core.utils.BasicDataConversionUtils;
import com.jaka.framework.core.dingding.config.APIInterfaceMapping;
import org.apache.commons.lang3.StringUtils;

import java.util.List;

/**
 * @author ：james.liu
 * @date ：Created in 2021/12/14 16:36
 * @description：
 * @version: 1.0
 */
public class AbstractAPIInput implements AbstractToJsonString {

    /**
     * 描述：接口名称
     */
    private String CmdId = null;

    /**
     * 描述：获取 接口名称
     */
    public String getCmdId() {
        return CmdId;
    }

    /**
     * 描述：设置 接口名称，自动设置
     */
    public void setCmdId(String cmdId) {
        CmdId = cmdId;
    }

    /**
     * 通过内省方式获取 ，JavaBean属性
     *
     * @param key
     * @return
     */
    private final Object get(final String key) {
        Object r = null;
        try {
            r = BeanUtils.getProperty(key, this);
        } catch (Exception e) {
        }
        return r;
    }

    /**
     * 校验必填 参数 是否 合法，系统自动调用
     *
     * @throws EmptyValueException 校验失败
     */
    public final void checkParameter() throws EmptyValueException {
        List<String> requiredList = APIInterfaceMapping.getRequiredListInput(getCmdId());
        if (requiredList == null) {
            return;
        }
        String value = null;
        for (String key : requiredList) {
            value = BasicDataConversionUtils.toString(get(key), null);
            if (StringUtils.isEmpty(value)) {
                throw new EmptyValueException(key + " is null");
            }
        }
    }
}
