package com.jaka.framework.core.dingding.api.addressbook.role.result;

import com.jaka.framework.core.dingding.base.AbstractAPIResult;

/**
 * @author ：james.liu
 * @date ：Created in 2021/12/27 10:48
 * @description：获取角色组列表出参
 * @version: 1.0
 */
public class TopapiRoleGetRoleGroupResult extends AbstractAPIResult {
}
