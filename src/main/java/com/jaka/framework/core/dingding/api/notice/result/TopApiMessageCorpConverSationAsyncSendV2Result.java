package com.jaka.framework.core.dingding.api.notice.result;

import com.jaka.framework.core.dingding.base.AbstractAPIResult;

/**
 * @author ：james.liu
 * @date ：Created in 2021/12/17 11:50
 * @description：发送工作通知出参
 * @version: 1.0
 */
public class TopApiMessageCorpConverSationAsyncSendV2Result extends AbstractAPIResult {

    /**
     * 请求ID
     */
    private String requestId;
    /**
     * 创建的异步发送任务ID。
     */
    private Long taskId;

    @Override
    public String getRequestId() {
        return requestId;
    }

    @Override
    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public Long getTaskId() {
        return taskId;
    }

    public void setTaskId(Long taskId) {
        this.taskId = taskId;
    }
}
