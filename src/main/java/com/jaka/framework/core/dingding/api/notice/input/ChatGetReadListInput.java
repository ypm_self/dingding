package com.jaka.framework.core.dingding.api.notice.input;

import com.jaka.framework.core.dingding.base.AbstractAPIResult;

/**
 * @author ：james.liu
 * @date ：Created in 2021/12/29 15:32
 * @description：查询群消息已读人员列表
 * @version: 1.0
 */
public class ChatGetReadListInput extends AbstractAPIResult {


}
