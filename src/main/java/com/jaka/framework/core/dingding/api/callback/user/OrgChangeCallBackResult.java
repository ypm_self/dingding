package com.jaka.framework.core.dingding.api.callback.user;

import com.jaka.framework.core.dingding.api.callback.CallBackUserBaseResult;

/**
 * @author ：james.liu
 * @date ：Created in 2021/12/30 11:23
 * @description：企业信息发生变更
 * @version: 1.0
 */
public class OrgChangeCallBackResult extends CallBackUserBaseResult {
}
