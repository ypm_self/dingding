package com.jaka.framework.core.dingding.api.addressbook.role.result;

import com.jaka.framework.core.dingding.base.AbstractAPIResult;

import java.util.List;

/**
 * @author ：james.liu
 * @date ：Created in 2021/12/27 10:50
 * @description：获取角色列表
 * @version: 1.0
 */
public class TopApiRoleListResult extends AbstractAPIResult {

    /**
     * 结果列表。
     */
    private TopApiRoleList result;

    public TopApiRoleList getResult() {
        return result;
    }

    public void setResult(TopApiRoleList result) {
        this.result = result;
    }

    public static class TopApiRoleList {
        /**
         * 是否还有更多数据。
         */
        private boolean hasMore;
        /**
         * 角色组列表。
         */
        private List<OpenRoleGroup> list;

        public boolean isHasMore() {
            return hasMore;
        }

        public void setHasMore(boolean hasMore) {
            this.hasMore = hasMore;
        }

        public List<OpenRoleGroup> getList() {
            return list;
        }

        public void setList(List<OpenRoleGroup> list) {
            this.list = list;
        }
    }

    public static class OpenRoleGroup {
        /**
         * 角色组名称。
         */
        private String name;
        /**
         * 角色组ID。
         */
        private Long groupId;
        /**
         * 角色列表。
         */
        private List<OpenRole> roles;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Long getGroupId() {
            return groupId;
        }

        public void setGroupId(Long groupId) {
            this.groupId = groupId;
        }

        public List<OpenRole> getRoles() {
            return roles;
        }

        public void setRoles(List<OpenRole> roles) {
            this.roles = roles;
        }
    }

    public static class OpenRole {
        /**
         * 角色名称。
         */
        private String name;
        /**
         * 角色ID。
         */
        private Long id;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }
    }
}
