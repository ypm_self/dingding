package com.jaka.framework.core.dingding.api.notice.input.message;

import java.util.List;

/**
 * @author ：james.liu
 * @date ：Created in 2021/12/29 14:20
 * @description：卡片消息
 * @version: 1.0
 */
public class NoticeActionCard  extends NoticeBaseModel{
    /**
     *
     */
    private ActionCard action_card;

    public ActionCard getAction_card() {
        return action_card;
    }

    public void setAction_card(ActionCard action_card) {
        this.action_card = action_card;
    }

    public static class ActionCard {
        /**
         * 透出到会话列表和通知的文案。
         */
        private String title;
        /**
         *消息内容，支持markdown，语法参考标准markdown语法。建议1000个字符以内
         */
        private String markdown;
        /**
         *使用整体跳转ActionCard样式时的标题。必须与single_url同时设置，最长20个字符。
         */
        private String single_title;
        /**
         *消息点击链接地址，当发送消息为小程序时支持小程序跳转链接，最长500个字符。
         */
        private String single_url;
        /**
         *使用独立跳转ActionCard样式时的按钮排列方式：
         * 0：竖直排列
         * 1：横向排列
         * 必须与btn_json_list同时设置。
         */
        private String btn_orientation;
        /**
         *使用独立跳转ActionCard样式时的按钮列表；必须与btn_orientation同时设置，且长度不超过1000字符。
         */
        private List<BtnJsonList> btn_json_list;

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getMarkdown() {
            return markdown;
        }

        public void setMarkdown(String markdown) {
            this.markdown = markdown;
        }

        public String getSingle_title() {
            return single_title;
        }

        public void setSingle_title(String single_title) {
            this.single_title = single_title;
        }

        public String getSingle_url() {
            return single_url;
        }

        public void setSingle_url(String single_url) {
            this.single_url = single_url;
        }

        public String getBtn_orientation() {
            return btn_orientation;
        }

        public void setBtn_orientation(String btn_orientation) {
            this.btn_orientation = btn_orientation;
        }

        public List<BtnJsonList> getBtn_json_list() {
            return btn_json_list;
        }

        public void setBtn_json_list(List<BtnJsonList> btn_json_list) {
            this.btn_json_list = btn_json_list;
        }
    }


    public static class BtnJsonList {

        private String title;

        private String action_url;

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getAction_url() {
            return action_url;
        }

        public void setAction_url(String action_url) {
            this.action_url = action_url;
        }
    }


}
