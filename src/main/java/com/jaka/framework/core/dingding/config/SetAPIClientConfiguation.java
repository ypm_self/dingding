package com.jaka.framework.core.dingding.config;

import com.jaka.framework.common.core.base.OKHttpUtils;
import com.jaka.framework.core.dingding.api.DingTalkAPIClient;

public abstract class SetAPIClientConfiguation extends APIClientConfiguration {
    
    public SetAPIClientConfiguation(){
    
    }
    
    @Override
    public DingTalkAPIClient build() {
        autoSetOkHttpClientIfOkHttpClientIsNull();
        setClientConfiguration();
        return new DingTalkAPIClient(this, new OKHttpUtils(okHttpClient));
    }
}
