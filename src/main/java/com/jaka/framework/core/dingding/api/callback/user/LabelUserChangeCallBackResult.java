package com.jaka.framework.core.dingding.api.callback.user;

import com.jaka.framework.core.dingding.api.callback.CallBackUserBaseResult;

import java.util.List;

/**
 * @author ：james.liu
 * @date ：Created in 2021/12/30 11:14
 * @description：员工角色信息发生变更
 * @version: 1.0
 */
public class LabelUserChangeCallBackResult  extends CallBackUserBaseResult {

    /**
     *员工角色发生变更的userId列表。
     */
    private List<String> UserIdList;
    /**
     * 角色或者角色组id列表。
     */
    private List<Long> LabelIdList;
    /**
     * 变更类型。
     */
    private String action;

    public List<String> getUserIdList() {
        return UserIdList;
    }

    public void setUserIdList(List<String> userIdList) {
        UserIdList = userIdList;
    }

    public List<Long> getLabelIdList() {
        return LabelIdList;
    }

    public void setLabelIdList(List<Long> labelIdList) {
        LabelIdList = labelIdList;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }
}
