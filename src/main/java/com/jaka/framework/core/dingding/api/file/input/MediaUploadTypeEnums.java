package com.jaka.framework.core.dingding.api.file.input;

/**
 * @author ：james.liu
 * @date ：Created in 2021/12/29 10:34
 * @description：媒体文件类型
 * @version: 1.0
 */
public enum MediaUploadTypeEnums {

    image("image", "图片，图片最大1MB。支持上传jpg、gif、png、bmp格式"),
    voice("voice", "语音，语音文件最大2MB。支持上传amr、mp3、wav格式"),
    video("video", "视频，视频最大10MB。支持上传mp4格式"),
    file("file", "普通文件，最大10MB。支持上传doc、docx、xls、xlsx、ppt、pptx、zip、pdf、rar格式");

    /**
     * @param type
     * @param desc
     */
    MediaUploadTypeEnums(String type, String desc) {
        this.type = type;
        this.desc = desc;
    }

    private String type;

    private String desc;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
