package com.jaka.framework.core.dingding.api.user.input;

import com.jaka.framework.core.dingding.base.AbstractAPIInput;

/**
 * @author ：james.liu
 * @date ：Created in 2021/12/16 16:25
 * @description：免登开发入参
 * @version: 1.0
 */
public class GetUserInfoInput extends AbstractAPIInput {

    /**
     * 获取的临时授权码
     */
    private String accessToken;

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

}
