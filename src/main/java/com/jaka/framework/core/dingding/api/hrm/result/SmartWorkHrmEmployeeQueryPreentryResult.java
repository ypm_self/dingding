package com.jaka.framework.core.dingding.api.hrm.result;

import com.jaka.framework.core.dingding.base.AbstractAPIResult;

import java.util.List;

/**
 * @author ：james.liu
 * @date ：Created in 2021/12/24 10:01
 * @description：获取待入职员工列表出参
 * @version: 1.0
 */
public class SmartWorkHrmEmployeeQueryPreentryResult extends AbstractAPIResult {

    /**
     * 返回结果。
     */
    private HrmEmployeeQueryPreentry result;

    public HrmEmployeeQueryPreentry getResult() {
        return result;
    }

    public void setResult(HrmEmployeeQueryPreentry result) {
        this.result = result;
    }

    public static class HrmEmployeeQueryPreentry {
        /**
         * 下一次分页调用的offset值，当返回结果里没有nextCursor时，表示分页结束。
         */
        private Integer next_cursor;
        /**
         * 查询到的待入职员工userid。
         */
        private List<String> data_list;

        public Integer getNext_cursor() {
            return next_cursor;
        }

        public void setNext_cursor(Integer next_cursor) {
            this.next_cursor = next_cursor;
        }

        public List<String> getData_list() {
            return data_list;
        }

        public void setData_list(List<String> data_list) {
            this.data_list = data_list;
        }
    }

}
