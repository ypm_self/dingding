package com.jaka.framework.core.dingding.api.chat.input;

import com.jaka.framework.core.dingding.base.AbstractAPIInput;

/**
 * @author ：james.liu
 * @date ：Created in 2021/12/30 17:32
 * @description：查询群成员
 * @version: 1.0
 */
public class TopApiImChatSceneGroupMemberGetInput extends AbstractAPIInput {
}
