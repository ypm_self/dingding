package com.jaka.framework.core.dingding.api.addressbook.dept.result;

import com.jaka.framework.core.dingding.base.AbstractAPIResult;

import java.util.List;

/**
 * @author ：james.liu
 * @date ：Created in 2021/12/20 9:03
 * @description：获取子部门ID列表
 * @version: 1.0
 */
public class TopapiV2DepartmentListsubidResult extends AbstractAPIResult {

    /**
     * 返回结果。
     */
    private DeptListSubIdResponse result;

    public DeptListSubIdResponse getResult() {
        return result;
    }

    public void setResult(DeptListSubIdResponse result) {
        this.result = result;
    }

    public static class DeptListSubIdResponse {
        /**
         * 子部门列表
         */
        private List<Long> deptIdList;

        public List<Long> getDeptIdList() {
            return deptIdList;
        }

        public void setDeptIdList(List<Long> deptIdList) {
            this.deptIdList = deptIdList;
        }
    }

}
