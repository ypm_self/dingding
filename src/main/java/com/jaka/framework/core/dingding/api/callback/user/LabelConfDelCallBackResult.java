package com.jaka.framework.core.dingding.api.callback.user;

import com.jaka.framework.core.dingding.api.callback.CallBackUserBaseResult;

import java.util.List;

/**
 * @author ：james.liu
 * @date ：Created in 2021/12/30 11:19
 * @description：删除角色或者角色组
 * @version: 1.0
 */
public class LabelConfDelCallBackResult extends CallBackUserBaseResult {

    /**
     * 角色或者角色组id列表。
     */
    private List<Long> LabelIdList;
    /**
     *
     */
    private String scope;

    public List<Long> getLabelIdList() {
        return LabelIdList;
    }

    public void setLabelIdList(List<Long> labelIdList) {
        LabelIdList = labelIdList;
    }

    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }
}
