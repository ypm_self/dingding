package com.jaka.framework.core.dingding.api.addressbook.user.input;

import com.jaka.framework.core.dingding.base.AbstractAPIInput;
import okhttp3.FormBody;
import okhttp3.RequestBody;

import java.util.List;
import java.util.Objects;

/**
 * @author ：james.liu
 * @date ：Created in 2022/1/6 13:52
 * @description：获取未登录钉钉的员工列表
 * @version: 1.0
 */
public class TopApinactiveUserV2GetInput extends AbstractAPIInput {

    /**
     * 是否活跃：
     * false：未登录
     * true：登录
     */
    private boolean isActive = false;
    /**
     * 部门ID列表，可调用获取部门列表获取，不传表示查询整个企业。
     */
    private List<Integer> deptIds;
    /**
     * 支持分页查询，与size参数同时设置时才生效，此参数代表偏移量，偏移量从0开始。
     */
    private Long offset = 1L;
    /**
     * 支持分页查询，与offset参数同时设置时才生效，此参数代表分页大小，最大100。
     */
    private Long size = 100L;
    /**
     * 查询日期，日期格式为：yyyyMMdd。
     */
    private String queryDate = "20200101";


    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public List<Integer> getDeptIds() {
        return deptIds;
    }

    public void setDeptIds(List<Integer> deptIds) {
        this.deptIds = deptIds;
    }

    public Long getOffset() {
        return offset;
    }

    public void setOffset(Long offset) {
        this.offset = offset;
    }

    public Long getSize() {
        return size;
    }

    public void setSize(Long size) {
        this.size = size;
    }

    public String getQueryDate() {
        return queryDate;
    }

    public void setQueryDate(String queryDate) {
        this.queryDate = queryDate;
    }

    /**
     * 描述：转换RequestBody对象
     */
    public final RequestBody toRequestBody() {
        RequestBody r = null;
        final FormBody.Builder builder = new FormBody.Builder();
        builder.add("is_active", String.valueOf(this.isActive));
        if(Objects.nonNull(this.getDeptIds())){
            builder.add("dept_ids", String.valueOf(this.getDeptIds()));
        }
        builder.add("offset", String.valueOf(this.getOffset()));
        builder.add("size", String.valueOf(this.getSize()));
        builder.add("query_date", this.getQueryDate());
        r = builder.build();
        return r;
    }
}
