package com.jaka.framework.core.dingding.api.user.input;

import com.jaka.framework.core.dingding.api.addressbook.dept.input.TopapiV2DepartmentListsubidInput;

/**
 * @author ：james.liu
 * @date ：Created in 2021/12/20 9:16
 * @description：获取部门用户userid列表；说明由于这部分的入参和获取子部门ID列表一致，所以直接继承
 * @version: 1.0
 */
public class TopApiUserListid extends TopapiV2DepartmentListsubidInput {


}
