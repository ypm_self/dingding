package com.jaka.framework.core.dingding.api.notice.input;

import com.jaka.framework.core.dingding.base.AbstractAPIInput;
import okhttp3.FormBody;
import okhttp3.RequestBody;

/**
 * @author ：james.liu
 * @date ：Created in 2021/12/29 15:08
 * @description：获取工作通知消息的发送进度
 * @version: 1.0
 */
public class TopApiMessageCorpConverSationGetsendprogressInput extends AbstractAPIInput {

    /**
     * 发送消息时使用的微应用的AgentID。
     * 企业内部应用可在开发者后台的应用详情页面查看。
     * 第三方企业应用可调用获取企业授权信息接口获取。
     */
    private Long agentId;
    /**
     * 工作通知任务ID，
     * 企业内部应用调用发送工作通知接口获取。
     * 钉钉三方企业应用调用发送工作通知接口获取。
     */
    private Long taskId;

    public Long getAgentId() {
        return agentId;
    }

    public void setAgentId(Long agentId) {
        this.agentId = agentId;
    }

    public Long getTaskId() {
        return taskId;
    }

    public void setTaskId(Long taskId) {
        this.taskId = taskId;
    }


    /**
     * 描述：转换RequestBody对象
     */
    public final RequestBody toRequestBody() {
        final FormBody.Builder builder = new FormBody.Builder();
        builder.add("agent_id", this.getAgentId().toString());
        builder.add("task_id", this.getTaskId().toString());
        RequestBody r = builder.build();
        return r;
    }
}
