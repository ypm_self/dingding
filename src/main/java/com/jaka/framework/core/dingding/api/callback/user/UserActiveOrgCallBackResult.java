package com.jaka.framework.core.dingding.api.callback.user;

import com.jaka.framework.core.dingding.api.callback.CallBackUserBaseResult;

import java.util.List;

/**
 * @author ：james.liu
 * @date ：Created in 2021/12/30 11:02
 * @description：通讯录事件-加入企业后用户激活
 * @version: 1.0
 */
public class UserActiveOrgCallBackResult extends CallBackUserBaseResult {

    /**
     * 用户发生变更的userId列表
     */
    private List<String> UserId;

    public List<String> getUserId() {
        return UserId;
    }

    public void setUserId(List<String> userId) {
        UserId = userId;
    }
}
