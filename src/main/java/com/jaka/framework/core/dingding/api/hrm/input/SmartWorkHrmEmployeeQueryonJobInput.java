package com.jaka.framework.core.dingding.api.hrm.input;

import com.jaka.framework.core.dingding.base.AbstractAPIInput;
import okhttp3.FormBody;
import okhttp3.RequestBody;

/**
 * @author ：james.liu
 * @date ：Created in 2021/12/20 9:28
 * @description：获取在职员工列表
 * @version: 1.0
 */
public class SmartWorkHrmEmployeeQueryonJobInput extends AbstractAPIInput {

    /**
     *在职员工子状态筛选，可以查询多个状态。不同状态之间使用英文逗号分隔。
     * 2：试用期
     * 3：正式
     * 5：待离职
     * -1：无状态
     */
    private String statusList;

    /**
     *分页游标，从0开始。根据返回结果里的next_cursor是否为空来判断是否还有下一页，且再次调用时offset设置成next_cursor的值。
     */
    private Long offset = 0L;
    /**
     *分页大小，最大50。
     */
    private Long size = 50L;

    public String getStatusList() {
        return statusList;
    }

    public void setStatusList(String statusList) {
        this.statusList = statusList;
    }

    public Long getOffset() {
        return offset;
    }

    public void setOffset(Long offset) {
        this.offset = offset;
    }

    public Long getSize() {
        return size;
    }

    public void setSize(Long size) {
        this.size = size;
    }


    /**
     * 描述：转换RequestBody对象
     */
    public final RequestBody toRequestBody() {
        final FormBody.Builder builder = new FormBody.Builder();
        builder.add("status_list", this.getStatusList());
        builder.add("offset", this.getOffset().toString());
        builder.add("size", this.getSize().toString());
        RequestBody r = builder.build();
        return r;
    }
}
