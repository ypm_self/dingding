package com.jaka.framework.core.dingding.api.hrm.result;

import com.jaka.framework.core.dingding.base.AbstractAPIResult;

/**
 * @author ：james.liu
 * @date ：Created in 2021/12/20 9:57
 * @description：获取在职员工列表
 * @version: 1.0
 */
public class SmartWorkHrmEmployeeQueryonJobResult extends AbstractAPIResult {

    /**
     * 调用结果。
     */
    private Boolean success;

    /**
     * 返回结果。
     */
    private PageResult result;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public PageResult getResult() {
        return result;
    }

    public void setResult(PageResult result) {
        this.result = result;
    }

    public static class PageResult {

        /**
         * 查询到的员工userid。
         */
        private String dataList;

        /**
         * 下一次分页调用的offset值，
         * 当返回结果里没有next_cursor时，
         * 表示分页结束。
         */
        private Long nextCursor;

        public String getDataList() {
            return dataList;
        }

        public void setDataList(String dataList) {
            this.dataList = dataList;
        }

        public Long getNextCursor() {
            return nextCursor;
        }

        public void setNextCursor(Long nextCursor) {
            this.nextCursor = nextCursor;
        }
    }
}
