package com.jaka.framework.core.dingding.api.callback;

import com.jaka.framework.common.core.model.AbstractToJsonString;

/**
 * @author ：james.liu
 * @date ：Created in 2021/12/30 9:22
 * @description：
 * @version: 1.0
 */
public class CallBackBaseResult implements AbstractToJsonString {
    /**
     * 事件类型。
     */
    private String EventType;

    public String getEventType() {
        return EventType;
    }

    public void setEventType(String eventType) {
        EventType = eventType;
    }
}
