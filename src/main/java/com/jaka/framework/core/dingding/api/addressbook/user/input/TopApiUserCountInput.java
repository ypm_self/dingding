package com.jaka.framework.core.dingding.api.addressbook.user.input;

import com.jaka.framework.core.dingding.base.AbstractAPIInput;
import okhttp3.FormBody;
import okhttp3.RequestBody;

/**
 * @author ：james.liu
 * @date ：Created in 2021/12/27 18:32
 * @description：获取员工人数
 * @version: 1.0
 */
public class TopApiUserCountInput extends AbstractAPIInput {

    private boolean onlyActive = false;

    /**
     * 描述：转换RequestBody对象
     */
    public final RequestBody toRequestBody() {
        RequestBody r = null;
        final FormBody.Builder builder = new FormBody.Builder();
        builder.add("only_active", String.valueOf(this.onlyActive));
        r = builder.build();
        return r;
    }

    public boolean isOnlyActive() {
        return onlyActive;
    }

    public void setOnlyActive(boolean onlyActive) {
        this.onlyActive = onlyActive;
    }
}
