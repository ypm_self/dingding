package com.jaka.framework.core.dingding;


import com.jaka.framework.core.dingding.api.DingTalkAPIClient;
import com.jaka.framework.core.dingding.config.SetAPIClientConfiguration;

/**
 * @author ：james.liu
 * @date ：Created in 2021/12/14 17:48
 * @description：客户端构建
 * @version: 1.0
 */
public final class DingTalkAPIClientBuilder extends SetAPIClientConfiguration {


    public DingTalkAPIClientBuilder() {

    }

    public DingTalkAPIClient build() {
        return super.build();
    }


    /**
     * @return
     */
    public static DingTalkAPIClientBuilder getInstance() {
        return new DingTalkAPIClientBuilder();
    }

}
