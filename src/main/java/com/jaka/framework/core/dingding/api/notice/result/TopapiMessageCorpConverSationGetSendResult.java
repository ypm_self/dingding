package com.jaka.framework.core.dingding.api.notice.result;

import com.jaka.framework.core.dingding.base.AbstractAPIResult;

import java.util.List;

/**
 * @author ：james.liu
 * @date ：Created in 2021/12/29 15:14
 * @description：获取工作通知消息的发送结果
 * @version: 1.0
 */
public class TopapiMessageCorpConverSationGetSendResult extends AbstractAPIResult {

    /**
     * 返回结果。
     */
    private AsyncSendResult send_result;

    public AsyncSendResult getSend_result() {
        return send_result;
    }

    public void setSend_result(AsyncSendResult send_result) {
        this.send_result = send_result;
    }

    public static class AsyncSendResult {
        /**
         * 无效的userid。
         */
        private String[] invalid_user_id_list;
        /**
         * 因发送消息过于频繁或超量而被流控过滤后实际未发送的userid。
         * 未被限流的接收者仍会被成功发送。
         * 限流规则包括：
         * 给同一用户发相同内容消息一天仅允许一次
         * 同一个应用给同一个用户发送消息：
         * 如果是ISV接入方式，给同一用户发消息一天不得超过100次
         * 如果是企业接入方式，此上限为500
         */
        private String[] forbidden_user_id_list;
        /**
         * 发送失败的userid。
         */
        private String[] failed_user_id_list;
        /**
         * 已读消息的userid。
         */
        private String[] read_user_id_list;
        /**
         * 未读消息的userid。
         */
        private String[] unread_user_id_list;
        /**
         * 无效的部门ID。
         */
        private List<Long> invalid_dept_id_list;
        /**
         * 推送被禁止的具体原因。
         */
        private List<SendForbiddenModel> forbidden_list;

        public String[] getInvalid_user_id_list() {
            return invalid_user_id_list;
        }

        public void setInvalid_user_id_list(String[] invalid_user_id_list) {
            this.invalid_user_id_list = invalid_user_id_list;
        }

        public String[] getForbidden_user_id_list() {
            return forbidden_user_id_list;
        }

        public void setForbidden_user_id_list(String[] forbidden_user_id_list) {
            this.forbidden_user_id_list = forbidden_user_id_list;
        }

        public String[] getFailed_user_id_list() {
            return failed_user_id_list;
        }

        public void setFailed_user_id_list(String[] failed_user_id_list) {
            this.failed_user_id_list = failed_user_id_list;
        }

        public String[] getRead_user_id_list() {
            return read_user_id_list;
        }

        public void setRead_user_id_list(String[] read_user_id_list) {
            this.read_user_id_list = read_user_id_list;
        }

        public String[] getUnread_user_id_list() {
            return unread_user_id_list;
        }

        public void setUnread_user_id_list(String[] unread_user_id_list) {
            this.unread_user_id_list = unread_user_id_list;
        }

        public List<Long> getInvalid_dept_id_list() {
            return invalid_dept_id_list;
        }

        public void setInvalid_dept_id_list(List<Long> invalid_dept_id_list) {
            this.invalid_dept_id_list = invalid_dept_id_list;
        }

        public List<SendForbiddenModel> getForbidden_list() {
            return forbidden_list;
        }

        public void setForbidden_list(List<SendForbiddenModel> forbidden_list) {
            this.forbidden_list = forbidden_list;
        }
    }

    public static class SendForbiddenModel {
        /**
         * 流控code：
         * 143105表示企业自建应用每日推送给用户的消息超过上限
         * 143106表示企业自建应用推送给用户的消息重复
         */
        private String code;
        /**
         * 流控阀值。
         */
        private Long count;
        /**
         * 被流控员工的userid。
         */
        private String userid;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public Long getCount() {
            return count;
        }

        public void setCount(Long count) {
            this.count = count;
        }

        public String getUserid() {
            return userid;
        }

        public void setUserid(String userid) {
            this.userid = userid;
        }
    }

}
