package com.jaka.framework.core.dingding.api.notice.result;

import com.jaka.framework.core.dingding.base.AbstractAPIResult;

/**
 * @author ：james.liu
 * @date ：Created in 2021/12/29 15:30
 * @description：
 * @version: 1.0
 */
public class ChatSendResult extends AbstractAPIResult {

    /**
     * 加密的消息ID。
     */
    private String messageId;

    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }
}
