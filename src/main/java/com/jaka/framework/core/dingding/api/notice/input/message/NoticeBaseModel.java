package com.jaka.framework.core.dingding.api.notice.input.message;

import com.jaka.framework.common.core.model.AbstractToJsonString;

/**
 * @author ：james.liu
 * @date ：Created in 2021/12/29 9:57
 * @description：消息通知基类
 * @version: 1.0
 */
public class NoticeBaseModel implements AbstractToJsonString {

    /**
     * 消息类型
     * 文本消息 text
     * 图片消息 image
     * 语音消息 voice
     * 文件消息 file
     * 链接消息 link
     * OA消息 oa   注意 OA消息支持通过status_bar参数设置消息的状态文案和颜色，消息发送后可调用更新工作通知状态栏接口更新消息状态和颜色。
     * Markdown消息 markdown
     * 卡片消息 action_card
     * 返回参数
     */
    private MsgtypeEnums msgtype;

    public MsgtypeEnums getMsgtype() {
        return msgtype;
    }

    public void setMsgtype(MsgtypeEnums msgtype) {
        this.msgtype = msgtype;
    }
}
