package com.jaka.framework.core.dingding.api.notice.input.message;

/**
 * @author ：james.liu
 * @date ：Created in 2021/12/29 9:58
 * @description：钉钉消息发送文本类型消息
 * @version: 1.0
 */
public class NoticeText extends NoticeBaseModel {

    private Text text;

    public Text getText() {
        return text;
    }

    public void setText(Text text) {
        this.text = text;
    }

    public static class Text {
        /**
         * 消息内容，建议500字符以内。
         */
        private String content;

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }
    }
}
