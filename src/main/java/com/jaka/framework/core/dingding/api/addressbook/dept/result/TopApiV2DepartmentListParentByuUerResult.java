package com.jaka.framework.core.dingding.api.addressbook.dept.result;

import com.jaka.framework.core.dingding.base.AbstractAPIResult;

import java.util.List;

/**
 * @author ：james.liu
 * @date ：Created in 2021/12/27 16:20
 * @description：
 * @version: 1.0
 */
public class TopApiV2DepartmentListParentByuUerResult extends AbstractAPIResult {
    /**
     * 返回结果。
     */
    private DeptListParentByUserResponse result;

    public DeptListParentByUserResponse getResult() {
        return result;
    }

    public void setResult(DeptListParentByUserResponse result) {
        this.result = result;
    }

    public static class DeptListParentByUserResponse {
        /**
         * 父部门列表集合。
         */
        private List<DeptParentResponse> parent_list;

        public List<DeptParentResponse> getParent_list() {
            return parent_list;
        }

        public void setParent_list(List<DeptParentResponse> parent_list) {
            this.parent_list = parent_list;
        }
    }

    public static class DeptParentResponse {
        /**
         * 父部门列表。
         */
        private List<Long> parent_dept_id_list;

        public List<Long> getParent_dept_id_list() {
            return parent_dept_id_list;
        }

        public void setParent_dept_id_list(List<Long> parent_dept_id_list) {
            this.parent_dept_id_list = parent_dept_id_list;
        }
    }
}
