package com.jaka.framework.core.dingding.api.callback.user;

import com.jaka.framework.core.dingding.api.callback.CallBackUserBaseResult;

import java.util.List;

/**
 * @author ：james.liu
 * @date ：Created in 2021/12/30 10:50
 * @description：通讯录事件-通讯录用户增加
 * @version: 1.0
 */
public class UserAddOrgCallBackResult extends CallBackUserBaseResult {
    /**
     * 操作人的userId。
     */
    private String OptStaffId;
    /**
     * 用户发生变更的userId列表。
     */
    private List<String> UserId;

    public String getOptStaffId() {
        return OptStaffId;
    }

    public void setOptStaffId(String optStaffId) {
        OptStaffId = optStaffId;
    }

    public List<String> getUserId() {
        return UserId;
    }

    public void setUserId(List<String> userId) {
        UserId = userId;
    }
}
