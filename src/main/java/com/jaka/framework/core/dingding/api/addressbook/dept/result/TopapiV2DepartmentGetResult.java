package com.jaka.framework.core.dingding.api.addressbook.dept.result;

import com.jaka.framework.core.dingding.base.AbstractAPIResult;

import java.util.List;

/**
 * @author ：james.liu
 * @date ：Created in 2021/12/17 13:45
 * @description：获取部门详情出参
 * @version: 1.0
 */
public class TopapiV2DepartmentGetResult extends AbstractAPIResult {

    /**
     * 部门详情。
     */
    private DeptGetResponse result;

    public DeptGetResponse getResult() {
        return result;
    }

    public void setResult(DeptGetResponse result) {
        this.result = result;
    }

    /**
     * 部门详情。
     */
    public static class DeptGetResponse{
        /**
         * 当部门群已经创建后，是否有新人加入部门会自动加入该群：
         * true：自动加入群
         * false：不会自动加入群
         */
        private Boolean auto_add_user;
        /**
         * 是否同步创建一个关联此部门的企业群：
         * true：创建
         * false：不创建
         */
        private Boolean create_dept_group;
        /**
         * 部门群ID。
         */
        private String dept_group_chat_id;
        /**
         * 部门ID。
         */
        private Long dept_id;
        /**
         * 部门的主管userd列表。
         */
        private List<String> dept_manager_userid_list;
        /**
         * 当隐藏本部门时（即hide_dept为true时），配置的允许在通讯录中查看本部门的部门列表。
         */
        private List<Long> dept_permits;
        /**
         * 部门是否来自关联组织：
         * true：是
         * false：不是
         * 说明 第三方企业应用不返回该参数。
         */
        private Boolean from_union_org;
        /**
         * 部门群是否包含子部门：
         * true：包含
         * false：不包含
         */
        private Boolean group_contain_sub_dept;
        /**
         * 是否隐藏本部门：
         * <p>
         * true：隐藏部门，隐藏后本部门将不会显示在公司通讯录中
         * <p>
         * false：显示部门
         */
        private Boolean hide_dept;
        /**
         * 部门名称。
         */
        private String name;
        /**
         * 在父部门中的次序值。
         */
        private Long order;
        /**
         * 企业群群主userId。
         */
        private String org_dept_owner;
        /**
         * 是否限制本部门成员查看通讯录：
         * true：开启限制。开启后本部门成员只能看到限定范围内的通讯录
         * false：不限制
         */
        private Boolean outer_dept;
        /**
         * 当限制部门成员的通讯录查看范围时（即outer_dept为true时），配置的部门员工可见部门列表。
         */
        private List<Long> outer_permit_depts;
        /**
         * 当限制部门成员的通讯录查看范围时（即outer_dept为true时），配置的部门员工可见员工列表。
         */
        private List<String> outer_permit_users;
        /**
         * 父部门ID。
         */
        private Long parent_id;
        /**
         * 部门标识字段。
         * 说明 第三方企业应用不返回该参数
         */
        private String source_identifier;
        /**
         * 教育部门标签：
         * campus：校区
         * period：学段
         * grade：年级
         * class：班级
         * 说明 第三方企业应用不返回该参数。
         */
        private String tags;
        /**
         * 当隐藏本部门时（即hide_dept为true时），配置的允许在通讯录中查看本部门的员工列表。
         */
        private List<String> user_permits;

        public Boolean getAuto_add_user() {
            return auto_add_user;
        }

        public void setAuto_add_user(Boolean auto_add_user) {
            this.auto_add_user = auto_add_user;
        }

        public Boolean getCreate_dept_group() {
            return create_dept_group;
        }

        public void setCreate_dept_group(Boolean create_dept_group) {
            this.create_dept_group = create_dept_group;
        }

        public String getDept_group_chat_id() {
            return dept_group_chat_id;
        }

        public void setDept_group_chat_id(String dept_group_chat_id) {
            this.dept_group_chat_id = dept_group_chat_id;
        }

        public Long getDept_id() {
            return dept_id;
        }

        public void setDept_id(Long dept_id) {
            this.dept_id = dept_id;
        }

        public List<String> getDept_manager_userid_list() {
            return dept_manager_userid_list;
        }

        public void setDept_manager_userid_list(List<String> dept_manager_userid_list) {
            this.dept_manager_userid_list = dept_manager_userid_list;
        }

        public List<Long> getDept_permits() {
            return dept_permits;
        }

        public void setDept_permits(List<Long> dept_permits) {
            this.dept_permits = dept_permits;
        }

        public Boolean getFrom_union_org() {
            return from_union_org;
        }

        public void setFrom_union_org(Boolean from_union_org) {
            this.from_union_org = from_union_org;
        }

        public Boolean getGroup_contain_sub_dept() {
            return group_contain_sub_dept;
        }

        public void setGroup_contain_sub_dept(Boolean group_contain_sub_dept) {
            this.group_contain_sub_dept = group_contain_sub_dept;
        }

        public Boolean getHide_dept() {
            return hide_dept;
        }

        public void setHide_dept(Boolean hide_dept) {
            this.hide_dept = hide_dept;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Long getOrder() {
            return order;
        }

        public void setOrder(Long order) {
            this.order = order;
        }

        public String getOrg_dept_owner() {
            return org_dept_owner;
        }

        public void setOrg_dept_owner(String org_dept_owner) {
            this.org_dept_owner = org_dept_owner;
        }

        public Boolean getOuter_dept() {
            return outer_dept;
        }

        public void setOuter_dept(Boolean outer_dept) {
            this.outer_dept = outer_dept;
        }

        public List<Long> getOuter_permit_depts() {
            return outer_permit_depts;
        }

        public void setOuter_permit_depts(List<Long> outer_permit_depts) {
            this.outer_permit_depts = outer_permit_depts;
        }

        public List<String> getOuter_permit_users() {
            return outer_permit_users;
        }

        public void setOuter_permit_users(List<String> outer_permit_users) {
            this.outer_permit_users = outer_permit_users;
        }

        public Long getParent_id() {
            return parent_id;
        }

        public void setParent_id(Long parent_id) {
            this.parent_id = parent_id;
        }

        public String getSource_identifier() {
            return source_identifier;
        }

        public void setSource_identifier(String source_identifier) {
            this.source_identifier = source_identifier;
        }

        public String getTags() {
            return tags;
        }

        public void setTags(String tags) {
            this.tags = tags;
        }

        public List<String> getUser_permits() {
            return user_permits;
        }

        public void setUser_permits(List<String> user_permits) {
            this.user_permits = user_permits;
        }
    }
}
