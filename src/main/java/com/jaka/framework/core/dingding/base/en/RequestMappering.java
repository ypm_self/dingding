package com.jaka.framework.core.dingding.base.en;

/**
 * @author ：james.liu
 * @date ：Created in 2021/12/14 19:15
 * @description：API接口参数映射
 * @version: 1.0
 */
public enum RequestMappering {
    
    /**
     * 根据userId获取用户详情
     */
    topapi_v2_user_get("topapi/v2/user/get", "根据userId获取用户详情"),
    /**
     * 通过免登码获取用户信息
     */
    getuserinfo("user/getuserinfo", "通过免登码获取用户信息"),
    /**
     * 使用钉钉账号登录第三方网站
     */
    getuserinfo_bycode("sns/getuserinfo_bycode", "使用钉钉账号登录第三方网站"),
    /**
     * 钉钉内免登第三方网站
     */
    getbyunionid("topapi/user/getbyunionid", "钉钉内免登第三方网站"),
    /**
     *
     */
    gettoken("gettoken", ""),
    /**
     * 发送工作通知
     */
    top_api_message_corp_conver_sation_async_send_v2("topapi/message/corpconversation/asyncsend_v2", "发送工作通知"),
    /**
     * 获取部门详情
     */
    get_department("topapi/v2/department/get", "获取部门详情"),
    /**
     * 获取子部门ID列表
     */
    get_department_list("topapi/v2/department/listsubid", "获取子部门ID列表"),
    /**
     * 获取部门列表
     */
    topapi_v2_department_listsub("topapi/v2/department/listsub", "获取部门列表"),
    /**
     * 获取指定部门的所有父部门列表
     */
    topapi_v2_department_listparentbydept("topapi/v2/department/listparentbydept", "获取指定部门的所有父部门列表"),
    /**
     * 获取指定用户的所有父部门列表
     */
    topapi_v2_department_list_parentbyuser("topapi/v2/department/listparentbyuser", "获取指定用户的所有父部门列表"),
    
    /**
     * 获取部门用户userid列表
     */
    get_uset_listid("topapi/user/listid", "获取部门用户userid列表"),
    /**
     * 获取员工人数
     */
    topapi_user_count("topapi/user/count", "获取员工人数"),
    
    // 通讯录管理 -- 角色管理 start
    
    topapi_role_get_role_group("topapi/role/getrolegroup", "获取角色组列表"),
    /**
     *
     */
    topapi_role_list("topapi/role/list", "获取角色列表"),
    
    /**
     * 获取未登录钉钉的员工列表
     */
    topapi_inactive_user_v2_get("topapi/inactive/user/v2/get", "获取未登录钉钉的员工列表"),
    
    /**
     * 获取管理员列表
     */
    topapi_user_listadmin("topapi/user/listadmin", "获取管理员列表\n"),
    
    
    // 通讯录管理 -- 角色管理 end
    
    
    // 智能人事  start
    /**
     * 获取在职员工列表
     */
    get_employee_queryonjob("topapi/smartwork/hrm/employee/queryonjob", "获取在职员工列表"),
    /**
     * 获取待入职员工列表
     */
    hrm_employee_query_preentry("topapi/topapi/smartwork/hrm/employee/querypreentry", "获取待入职员工列表"),
    /**
     * 添加企业待入职员工添加企业待入职员工
     */
    hrm_employee_add_preentry("topapi/smartwork/hrm/employee/addpreentry", "添加企业待入职员工"),
    /**
     * 获取离职员工列表
     */
    hrm_employee_query_dimission("topapi/smartwork/hrm/employee/querydimission", "获取离职员工列表"),
    /**
     * 获取员工离职信息
     */
    hrm_employee_list_dimission("topapi/smartwork/hrm/employee/listdimission", "获取员工离职信息"),
    
    
    // 智能人事  end
    
    // 文件存储 start
    media_upload("media/upload", "上传媒体文件"),
    // 文件存储 end
    
    
    auth_scopes("auth/scopes", "获取通讯录权限范围"),
    
    /**
     * 通过免登码获取用户信息
     */
    top_api_v2_user_get_user_info("/topapi/v2/user/getuserinfo", "通过免登码获取用户信息"),;
    
    
    /**
     * 配置信息
     *
     * @param mapperName 接口地址
     * @param mapperDesc 接口描述
     */
    RequestMappering(String mapperName, String mapperDesc) {
        this.mapperDesc = mapperDesc;
        this.mapperName = mapperName;
    }
    
    /**
     * 接口地址
     */
    private String mapperName;
    
    /**
     * 接口描述
     */
    private String mapperDesc;
    
    public String getMapperName() {
        return mapperName;
    }
    
    public void setMapperName(String mapperName) {
        this.mapperName = mapperName;
    }
    
    public String getMapperDesc() {
        return mapperDesc;
    }
    
    public void setMapperDesc(String mapperDesc) {
        this.mapperDesc = mapperDesc;
    }
    
    /**
     * @param apppend
     * @return
     */
    public String getAppenMapper(String apppend) {
        return getMapperName() + apppend;
    }
}
