package com.jaka.framework.core.dingding.api.user.result;

import com.jaka.framework.core.dingding.base.AbstractAPIResult;

import java.util.List;

/**
 * @author ：james.liu
 * @date ：Created in 2021/12/20 9:20
 * @description：获取部门用户userid列表
 * @version: 1.0
 */
public class TopApiUserListIdResult extends AbstractAPIResult {

    /**
     * 返回结果。
     */
    private ListUserByDeptResponse result;

    public ListUserByDeptResponse getResult() {
        return result;
    }

    public void setResult(ListUserByDeptResponse result) {
        this.result = result;
    }

    public static class ListUserByDeptResponse {

        /**
         * 指定部门的userid列表。
         */
        private List<String> useridList;

        public List<String> getUseridList() {
            return useridList;
        }

        public void setUseridList(List<String> useridList) {
            this.useridList = useridList;
        }
    }

}
