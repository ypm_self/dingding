package com.jaka.framework.core.dingding.api.notice.result;

import com.jaka.framework.core.dingding.base.AbstractAPIResult;

/**
 * @author ：james.liu
 * @date ：Created in 2021/12/29 15:03
 * @description：更新工作通知状态栏
 * @version: 1.0
 */
public class TopApiMessageCorpConversAtionStatusBarUpdateResult extends AbstractAPIResult {

}
