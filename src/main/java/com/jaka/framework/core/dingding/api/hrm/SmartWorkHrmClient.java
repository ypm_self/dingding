package com.jaka.framework.core.dingding.api.hrm;

import com.jaka.framework.common.core.base.IHttpUtils;
import com.jaka.framework.core.dingding.api.hrm.input.*;
import com.jaka.framework.core.dingding.api.hrm.result.*;
import com.jaka.framework.core.dingding.base.DefaultAPIActionProxy;
import com.jaka.framework.core.dingding.base.en.RequestMappering;
import com.jaka.framework.core.dingding.config.APIClientConfiguration;

/**
 * @author ：james.liu
 * @date ：Created in 2021/12/24 10:25
 * @description：智能人事客户端
 * @version: 1.0
 */
public class SmartWorkHrmClient {

    private final IHttpUtils httpUtils;
    private final APIClientConfiguration configuration;

    public SmartWorkHrmClient(final APIClientConfiguration configuration, final IHttpUtils httpUtils) {
        this.configuration = configuration;
        this.httpUtils = httpUtils;
    }

    /**
     * 获取在职员工列表
     *
     * @param input
     * @return
     * @throws Exception
     */
    public SmartWorkHrmEmployeeQueryonJobResult hrmEmployeeQueryonJob(final SmartWorkHrmEmployeeQueryonJobInput input) throws Exception {
        input.setCmdId(RequestMappering.get_employee_queryonjob.getMapperName());
        return DefaultAPIActionProxy.getInstance().doAction(input,
                configuration,
                httpUtils,
                SmartWorkHrmEmployeeQueryonJobResult.class,
                input.toRequestBody());
    }

    /**
     * 获取待入职员工列表获取待入职员工列表
     *
     * @param input
     * @return
     * @throws Exception
     */
    public SmartWorkHrmEmployeeQueryPreentryResult hrmEmployeeQueryPreentry(final SmartWorkHrmEmployeeQueryPreentryInput input) throws Exception {
        input.setCmdId(RequestMappering.hrm_employee_query_preentry.getMapperName());
        return DefaultAPIActionProxy.getInstance().doAction(input,
                configuration,
                httpUtils,
                SmartWorkHrmEmployeeQueryPreentryResult.class,
                input.toRequestBody());
    }

    /**
     * 添加企业待入职员工
     *
     * @param input
     * @return
     * @throws Exception
     */
    public SmartWorkHrmEmployeeAddPreentryResult hrmEmployeeAddPreentry(final SmartWorkHrmEmployeeAddPreentryInput input) throws Exception {
        input.setCmdId(RequestMappering.hrm_employee_add_preentry.getMapperName());
        return DefaultAPIActionProxy.getInstance().doAction(input,
                configuration,
                httpUtils,
                SmartWorkHrmEmployeeAddPreentryResult.class,
                input.toRequestBody());
    }

    /**
     * 获取离职员工列表
     *
     * @param input
     * @return
     * @throws Exception
     */
    public SmartWorkHrmEmployeeQueryDimissionResult hrmEmployeeQueryDimission(final SmartWorkHrmEmployeeQueryDimissionInput input) throws Exception {
        input.setCmdId(RequestMappering.hrm_employee_query_dimission.getMapperName());
        return DefaultAPIActionProxy.getInstance().doAction(input,
                configuration,
                httpUtils,
                SmartWorkHrmEmployeeQueryDimissionResult.class,
                input.toRequestBody());
    }

    /**
     * 获取员工离职信息
     *
     * @param input
     * @return
     * @throws Exception
     */
    public SmartWorkHrmEmployeeListDimissionResult smartWorkHrmEmployeeListDimission(final SmartWorkHrmEmployeeListDimissionInput input) throws Exception {
        input.setCmdId(RequestMappering.hrm_employee_list_dimission.getMapperName());
        return DefaultAPIActionProxy.getInstance().doAction(input,
                configuration,
                httpUtils,
                SmartWorkHrmEmployeeListDimissionResult.class,
                input.toRequestBody());
    }

}
