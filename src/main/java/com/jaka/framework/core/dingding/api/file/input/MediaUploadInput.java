package com.jaka.framework.core.dingding.api.file.input;

import com.jaka.framework.core.dingding.base.AbstractAPIInput;

/**
 * @author ：james.liu
 * @date ：Created in 2021/12/29 10:34
 * @description：上传媒体文件
 * @version: 1.0
 */
public class MediaUploadInput extends AbstractAPIInput {

    /**
     *
     */
    private MediaUploadTypeEnums type;
//    /**
//     * 要上传的媒体文件。
//     */
//    private FileItem media;
//
//    public MediaUploadTypeEnums getType() {
//        return type;
//    }
//
//    public void setType(MediaUploadTypeEnums type) {
//        this.type = type;
//    }
//
//    public FileItem getMedia() {
//        return media;
//    }
//
//    public void setMedia(FileItem media) {
//        this.media = media;
//    }
//
//    /**
//     * 描述：转换RequestBody对象
//     */
//    public final RequestBody toRequestBody() {
//        final FormBody.Builder builder = new FormBody.Builder();
//        builder.add("type", this.type.getType());
//        builder.add("media", this.getMedia().getString());
//        RequestBody r = builder.build();
//        return r;
//    }
}
