package com.jaka.framework.core.dingding.api.addressbook.user.result;


import com.jaka.framework.core.dingding.base.AbstractAPIResult;

import java.io.Serializable;
import java.util.List;

/**
 * @author ：james.liu
 * @date ：Created in 2021/12/14 17:39
 * @description：
 * @version: 1.0
 */
public class TopApiV2UserGetResult extends AbstractAPIResult {

    private UserGetResponse result;

    public UserGetResponse getResult() {
        return result;
    }

    public void setResult(UserGetResponse result) {
        this.result = result;
    }

    public static class UserGetResponse implements Serializable {
        private static final long serialVersionUID = 8184313373849474869L;
        /**
         *是否激活了钉钉：
         * true：已激活
         * false：未激活
         */
        private Boolean active;
        /**
         *是否为企业的管理员：
         * true：是
         * false：不是
         */
        private Boolean admin;
        /**
         *头像。
         * 说明 员工使用默认头像，不返回该字段，手动设置头像会返回。
         */
        private String avatar;
        /**
         *是否为企业的老板：
         * true：是
         * false：不是
         */
        private Boolean boss;
        /**
         *所属部门ID列表。
         */
        private List<Long> dept_id_list;
        /**
         *员工在对应的部门中的排序。
         */
        private List<DeptOrder> dept_order_list;
        /**
         *员工的直属主管。
         */
        private String manager_userid;
        /**
         *员工邮箱。
         */
        private String email;
        /**
         *是否专属帐号。
         */
        private Boolean exclusive_account;
        /**
         *扩展属性，最大长度2000个字符。扩展属性，最大长度2000个字符。
         */
        private String extension;
        /**
         *是否号码隐藏：是否号码隐藏：
         */
        private Boolean hide_mobile;
        /**
         *入职时间，Unix时间戳，单位毫秒。
         */
        private Long hired_date;
        /**
         *员工工号。
         */
        private String job_number;
        /**
         *员工在对应的部门中是否领导。
         */
        private List<DeptLeader> leader_in_dept;
        /**
         *手机号码。手机号码。
         */
        private String mobile;
        /**
         *员工名称。
         */
        private String name;
        /**
         *员工的企业邮箱。
         */
        private String org_email;
        /**
         *是否完成了实名认证：
         * true：已认证
         * false：未认证
         */
        private Boolean real_authed;
        /**
         *备注备注
         */
        private String remark;
        /**
         *角色列表。
         */
        private List<UserRole> role_list;
        /**
         *是否为企业的高管：
         * true：是
         * false：不是
         */
        private Boolean senior;
        /**
         *国际电话区号。
         */
        private String state_code;
        /**
         *分机号。
         */
        private String telephone;
        /**
         *职位
         */
        private String title;
        /**
         *当用户来自于关联组织时的关联信息。
         */
        private UnionEmpExt union_emp_ext;
        /**
         *员工在当前开发者企业账号范围内的唯一标识。
         */
        private String unionid;
        /**
         *员工的userId。
         */
        private String userid;
        /**
         *办公地点。
         */
        private String work_place;

        public UserGetResponse() {
        }

        public Boolean getActive() {
            return active;
        }

        public void setActive(Boolean active) {
            this.active = active;
        }

        public Boolean getAdmin() {
            return admin;
        }

        public void setAdmin(Boolean admin) {
            this.admin = admin;
        }

        public String getAvatar() {
            return avatar;
        }

        public void setAvatar(String avatar) {
            this.avatar = avatar;
        }

        public Boolean getBoss() {
            return boss;
        }

        public void setBoss(Boolean boss) {
            this.boss = boss;
        }

        public List<Long> getDept_id_list() {
            return dept_id_list;
        }

        public void setDept_id_list(List<Long> dept_id_list) {
            this.dept_id_list = dept_id_list;
        }

        public List<DeptOrder> getDept_order_list() {
            return dept_order_list;
        }

        public void setDept_order_list(List<DeptOrder> dept_order_list) {
            this.dept_order_list = dept_order_list;
        }

        public String getManager_userid() {
            return manager_userid;
        }

        public void setManager_userid(String manager_userid) {
            this.manager_userid = manager_userid;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public Boolean getExclusive_account() {
            return exclusive_account;
        }

        public void setExclusive_account(Boolean exclusive_account) {
            this.exclusive_account = exclusive_account;
        }

        public String getExtension() {
            return extension;
        }

        public void setExtension(String extension) {
            this.extension = extension;
        }

        public Boolean getHide_mobile() {
            return hide_mobile;
        }

        public void setHide_mobile(Boolean hide_mobile) {
            this.hide_mobile = hide_mobile;
        }

        public Long getHired_date() {
            return hired_date;
        }

        public void setHired_date(Long hired_date) {
            this.hired_date = hired_date;
        }

        public String getJob_number() {
            return job_number;
        }

        public void setJob_number(String job_number) {
            this.job_number = job_number;
        }

        public List<DeptLeader> getLeader_in_dept() {
            return leader_in_dept;
        }

        public void setLeader_in_dept(List<DeptLeader> leader_in_dept) {
            this.leader_in_dept = leader_in_dept;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getOrg_email() {
            return org_email;
        }

        public void setOrg_email(String org_email) {
            this.org_email = org_email;
        }

        public Boolean getReal_authed() {
            return real_authed;
        }

        public void setReal_authed(Boolean real_authed) {
            this.real_authed = real_authed;
        }

        public String getRemark() {
            return remark;
        }

        public void setRemark(String remark) {
            this.remark = remark;
        }

        public List<UserRole> getRole_list() {
            return role_list;
        }

        public void setRole_list(List<UserRole> role_list) {
            this.role_list = role_list;
        }

        public Boolean getSenior() {
            return senior;
        }

        public void setSenior(Boolean senior) {
            this.senior = senior;
        }

        public String getState_code() {
            return state_code;
        }

        public void setState_code(String state_code) {
            this.state_code = state_code;
        }

        public String getTelephone() {
            return telephone;
        }

        public void setTelephone(String telephone) {
            this.telephone = telephone;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public UnionEmpExt getUnion_emp_ext() {
            return union_emp_ext;
        }

        public void setUnion_emp_ext(UnionEmpExt union_emp_ext) {
            this.union_emp_ext = union_emp_ext;
        }

        public String getUnionid() {
            return unionid;
        }

        public void setUnionid(String unionid) {
            this.unionid = unionid;
        }

        public String getUserid() {
            return userid;
        }

        public void setUserid(String userid) {
            this.userid = userid;
        }

        public String getWork_place() {
            return work_place;
        }

        public void setWork_place(String work_place) {
            this.work_place = work_place;
        }
    }

    /**
     * 当用户来自于关联组织时的关联信息。
     * <p>
     * 说明 用户所在企业存在关联关系的企业，才会返回该字段。
     */
    public static class UnionEmpExt implements Serializable {
        private static final long serialVersionUID = 2668569317591419849L;
        /**
         * 关联分支组织的企业corpId。
         */
        private String corpId;
        /**
         * 关联映射关系。
         */
        private List<UnionEmpMapVo> unionEmpMapList;
        /**
         * 关联分支组织中的员工userId。
         */
        private String userid;

        public UnionEmpExt() {
        }

        public String getCorpId() {
            return corpId;
        }

        public void setCorpId(String corpId) {
            this.corpId = corpId;
        }

        public List<UnionEmpMapVo> getUnionEmpMapList() {
            return unionEmpMapList;
        }

        public void setUnionEmpMapList(List<UnionEmpMapVo> unionEmpMapList) {
            this.unionEmpMapList = unionEmpMapList;
        }

        public String getUserid() {
            return userid;
        }

        public void setUserid(String userid) {
            this.userid = userid;
        }
    }

    /**
     *
     */
    public static class UnionEmpMapVo implements Serializable {
        private static final long serialVersionUID = 1L;
        private String corpId;

        private String userid;

        public String getCorpId() {
            return corpId;
        }

        public void setCorpId(String corpId) {
            this.corpId = corpId;
        }

        public String getUserid() {
            return userid;
        }

        public void setUserid(String userid) {
            this.userid = userid;
        }
    }

    /**
     * 角色列表。
     */
    public static class UserRole implements Serializable {
        private static final long serialVersionUID = 6886394561293367726L;
        /**
         * 角色组名称。
         */
        private String groupName;
        /**
         * 角色ID。
         */
        private Long id;
        /**
         * 角色名称。
         */
        private String name;

        public String getGroupName() {
            return groupName;
        }

        public void setGroupName(String groupName) {
            this.groupName = groupName;
        }

        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }

    /**
     * 员工在对应的部门中是否领导。
     */
    public static class DeptLeader implements Serializable {
        private static final long serialVersionUID = 1L;
        /**
         * 部门ID。
         */
        private Long deptId;
        /**
         * 是否是领导：
         * true：是
         * false：不是
         */
        private Boolean leader;

        public Long getDeptId() {
            return deptId;
        }

        public void setDeptId(Long deptId) {
            this.deptId = deptId;
        }

        public Boolean getLeader() {
            return leader;
        }

        public void setLeader(Boolean leader) {
            this.leader = leader;
        }
    }

    /**
     * 员工在对应的部门中的排序。
     */
    public static class DeptOrder implements Serializable {
        private static final long serialVersionUID = 1L;
        /**
         * 部门ID。
         */
        private Long deptId;

        /**
         * 员工在部门中的排序。
         */
        private Long order;

        public DeptOrder() {
        }

        public Long getDeptId() {
            return this.deptId;
        }

        public void setDeptId(Long deptId) {
            this.deptId = deptId;
        }

        public Long getOrder() {
            return this.order;
        }

        public void setOrder(Long order) {
            this.order = order;
        }
    }
}
