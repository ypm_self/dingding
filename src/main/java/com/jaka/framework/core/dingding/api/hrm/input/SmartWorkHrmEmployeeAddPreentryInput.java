package com.jaka.framework.core.dingding.api.hrm.input;

import com.alibaba.fastjson.JSON;
import com.jaka.framework.core.dingding.base.AbstractAPIInput;
import okhttp3.FormBody;
import okhttp3.RequestBody;

import java.util.Date;

/**
 * @author ：james.liu
 * @date ：Created in 2021/12/24 10:35
 * @description：添加企业待入职员工
 * @version: 1.0
 */
public class SmartWorkHrmEmployeeAddPreentryInput extends AbstractAPIInput {

    private HrmEmployeeAddPreentry param;

    public HrmEmployeeAddPreentry getParam() {
        return param;
    }

    public void setParam(HrmEmployeeAddPreentry param) {
        this.param = param;
    }

    public static class HrmEmployeeAddPreentry {
        /**
         * 预期入职时间。
         */
        private Date pre_entry_time;
        /**
         * 待入职员工姓名。
         */
        private String name;
        /**
         * 扩展信息，json串格式，按要求传入有效信息，无效信息不会保存。
         * 有效信息有：
         * depts：部门ID列表，多个部门用"|"分隔
         * mainDeptId：主部门ID
         * mainDeptName：主部门名称
         * position：职位
         * workPlace：工作地点
         * jobNumber：工号
         * employeeType：员工类型枚举值：
         * 0：无类型
         * 1：全职
         * 2：兼职
         * 3：实习
         * 4：劳务派遣
         * 5：退休返聘
         * 6：劳务外包
         */
        private String extend_info;
        /**
         * 操作人userid。
         */
        private String op_userid;
        /**
         * 待入职员工手机号。
         * 说明 本接口只能添加非本企业员工（手机号为准），否则报错系统繁忙。
         */
        private String mobile;

        public Date getPre_entry_time() {
            return pre_entry_time;
        }

        public void setPre_entry_time(Date pre_entry_time) {
            this.pre_entry_time = pre_entry_time;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getExtend_info() {
            return extend_info;
        }

        public void setExtend_info(String extend_info) {
            this.extend_info = extend_info;
        }

        public String getOp_userid() {
            return op_userid;
        }

        public void setOp_userid(String op_userid) {
            this.op_userid = op_userid;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }
    }

    /**
     * 描述：转换RequestBody对象
     */
    public final RequestBody toRequestBody() {
        HrmEmployeeAddPreentry param = this.getParam();
        final FormBody.Builder builder = new FormBody.Builder();
        builder.add("param", JSON.toJSONString(param));
        RequestBody r = builder.build();
        return r;
    }
}
