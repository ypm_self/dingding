package com.jaka.framework.core.dingding.api.callback.bpms;

import com.jaka.framework.core.dingding.api.callback.CallBackBaseResult;

/**
 * @author ：james.liu
 * @date ：Created in 2021/12/30 11:31
 * @description：
 * @version: 1.0
 */
public class BpmsTaskChangeBaseResult extends CallBackBaseResult {
    /**
     * 审批实例对应的企业。
     */
    private String corpId;
    /**
     * 审批实例id。
     */
    private String processInstanceId;
    /**
     * 实例标题。
     */
    private String title;
    /**
     * finish：审批正常结束（同意或拒绝）
     * terminate：审批终止（发起人撤销审批单）
     */
    private String type;
    /**
     * 实例创建时间。
     */
    private Long createTime;
    /**
     * 发起审批实例的员工。
     */
    private String staffId;
    /**
     * 审批模板的唯一码。
     */
    private String processCode;


    public String getCorpId() {
        return corpId;
    }

    public void setCorpId(String corpId) {
        this.corpId = corpId;
    }

    public String getProcessInstanceId() {
        return processInstanceId;
    }

    public void setProcessInstanceId(String processInstanceId) {
        this.processInstanceId = processInstanceId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    public String getStaffId() {
        return staffId;
    }

    public void setStaffId(String staffId) {
        this.staffId = staffId;
    }

    public String getProcessCode() {
        return processCode;
    }

    public void setProcessCode(String processCode) {
        this.processCode = processCode;
    }

}
