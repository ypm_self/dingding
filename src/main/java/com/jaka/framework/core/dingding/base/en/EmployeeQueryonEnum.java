package com.jaka.framework.core.dingding.base.en;

/**
 * @author ：james.liu
 * @date ：Created in 2021/12/20 9:52
 * @description：在职员工子状态
 * @version: 1.0
 */
public enum EmployeeQueryonEnum {

    trialPeriod(2, "试用期"),

    Formal(3, "正式"),

    toBeResigned(5, "待离职"),

    noState(-1, "无状态");

    private int status;

    private String desc;

    EmployeeQueryonEnum(int status, String desc) {
        this.status = status;
        this.desc = desc;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
